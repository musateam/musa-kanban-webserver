namespace Musa.Importer

module Metric = 

    let add (data:Caiq.Model.ValueNode3, providerId) = 
        let db = Db.getDb
        let metric = db.Musa.CspMetrics.Create()
        metric.SlaId <- data.Id
        metric.Answer <- data.Answer.Value
        metric.Question <- data.Question
        metric.Value <- match data.Value with
                        | true -> sbyte 1
                        | _ -> sbyte 0
        metric.ProviderId <- providerId
        db.SubmitUpdates()
        metric

    let get (providerId, slaId) = 
        let db = Db.getDb
        query {
            for metric in db.Musa.CspMetrics do 
            where (metric.ProviderId = providerId)
            where (metric.SlaId = slaId)
            headOrDefault
        }

    let addOrUpdate (data:Caiq.Model.ValueNode3, providerId) = 
        let db = Db.getDb
        let metric = get (providerId, data.Id)
        match metric with
        | null -> add (data, providerId)
        | _ -> metric.SlaId <- data.Id
               metric.Answer <- data.Answer.Value
               metric.Question <- data.Question
               metric.Value <- match data.Value with
                               | true -> sbyte 1
                               | _ -> sbyte 0
               metric.ProviderId <- providerId
               db.SubmitUpdates()
               metric

    let list providerId = 
        let db = Db.getDb
        query {
            for metric in db.Musa.CspMetrics do
            where (metric.ProviderId = providerId)
            select metric
        }

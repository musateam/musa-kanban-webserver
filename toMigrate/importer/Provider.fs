namespace Musa.Importer

open Newtonsoft.Json

module Provider =

        let add providerName = 
            let db = Db.getDb
            let newProvider = db.Musa.CspProviders.Create()
            newProvider.Name <- providerName
            db.SubmitUpdates()
            newProvider

        let getId providerName = 
            let db = Db.getDb
            let q = 
                query {
                    for provider in db.Musa.CspProviders do
                    where (provider.Name = providerName)
                    headOrDefault
                }
            match q with 
            | null -> let newProvider = add providerName
                      newProvider.ProviderId
            | _ ->  q.ProviderId

        let list = 
            let db = Db.getDb
            query {
                for provider in db.Musa.CspProviders do
                select provider
            }
            
        let listNames = 
            list |> Seq.map(fun v -> v.Name)

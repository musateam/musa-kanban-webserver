namespace Musa.Importer

open MySql.Data
open FSharp.Data.Sql

module Db =

    [<Literal>]
    let ResPath = __SOURCE_DIRECTORY__ + @"/../../packages/MySql.Data.6.9.9/lib/net45"

    type Sql = SqlDataProvider<DatabaseVendor = Common.DatabaseProviderTypes.MYSQL,
                                ConnectionString = "Server=localhost;Database=musa;User=root;Password=root",
                                ResolutionPath = ResPath,
                                IndividualsAmount = 1000,
                                UseOptionTypes = true,
                                Owner = "musa">

    let getDb = Sql.GetDataContext()

    let musa = getDb.Musa
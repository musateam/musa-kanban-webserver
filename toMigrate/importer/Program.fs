namespace Musa.Importer

open System

module Main = 
    [<EntryPoint>]
    let main argv = 
        let xml = Caiq.Model.Load argv.[1]

        for main in xml.Root.ValueNodes do
            for sub in main.ValueNodes do
                for child in sub.ValueNodes do
                    let p = Provider.getId argv.[0]
                    Metric.addOrUpdate (child, Provider.getId argv.[0]) |> ignore
                    printfn "Processing metric %s for provider %s" child.Id argv.[0]
        
        0

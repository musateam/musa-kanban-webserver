namespace Musa.Importer

open FSharp.Data
open System.Xml.Linq

module Caiq = 
    type Model = XmlProvider<"data/CloudSigma-CAIQ-v1.2.1-2013-07-30.xml">

    let getSampleData = Model.GetSample()


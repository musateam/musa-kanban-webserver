#r "../packages/FSharp.Data/lib/net40/FSharp.Data.dll"
#r "../packages/Hekate/lib/net35/Hekate.dll"
#r "../packages/Aether/lib/net35/Aether.dll"

#r "../packages/rda.SocketsForPCL/lib/net45/Sockets.Plugin.dll"
#r "../packages/rda.SocketsForPCL/lib/net45/Sockets.Plugin.Abstractions.dll"
#r "../packages/Neo4j.Driver/lib/netstandard1.3/Neo4j.Driver.dll"

open Hekate
open Aether
open FSharp.Data
open System

open Neo4j.Driver.V1
open Sockets.Plugin.Abstractions

let path = "/Users/jacek/Documents/Development/musa/CCM2Nist.csv"
let fileData = CsvFile.Load("/Users/jacek/Documents/Development/musa/CCM2Nist.csv", ";").Cache()

let sample = fileData.Rows |> Seq.take(5)

let nist = sample |> Seq.map(fun row -> row.GetColumn "NIST")

let generateNode (payload:string) = 
    (payload, payload)

type Node = string * string
type Edge = string * string * string

(nist |> Seq.item 1).Split [|','|] |> Array.map generateNode |> Array.toList


let session = driver.Session()

let GetData = 
    let up = AuthTokens.Basic("neo4j", "musa")
    use driver = GraphDatabase.Driver("bolt://127.0.0.1:7687", up)
    use session = driver.Session()
    let o = session.Run("MATCH (r) return r")


module JWT =
    open System
    open System.Text
    open System.Text.RegularExpressions
    open System.Security.Cryptography
    let replace (oldVal: string) (newVal: string) = fun (s: string) -> s.Replace(oldVal, newVal)
    let minify = 
        let regex = Regex("(\"(?:[^\"\\\\]|\\\\.)*\")|\\s+", RegexOptions.Compiled|||RegexOptions.CultureInvariant)
        fun s ->
            regex.Replace(s, "$1")
    let base64UrlEncode bytes =
        Convert.ToBase64String(bytes) |> replace "+" "-" |> replace "/" "_" |> replace "=" ""
    type IJwtAuthority =
        inherit IDisposable
        abstract member IssueToken: header:string -> payload:string -> string
        abstract member VerifyToken: string -> bool
    let newJwtAuthority (initAlg: byte array -> HMAC) key =
        let alg = initAlg(key)
        let encode = minify >> Encoding.UTF8.GetBytes >> base64UrlEncode
        let issue header payload =
            let parts = [header; payload] |> List.map encode |> String.concat "."
            let signature = parts |> Encoding.UTF8.GetBytes |> alg.ComputeHash |> base64UrlEncode
            [parts; signature] |> String.concat "."
        let verify (token: string) =
            let secondDot = token.LastIndexOf(".")
            let parts = token.Substring(0, secondDot)
            let signature = token.Substring(secondDot + 1)
            (parts |> Encoding.UTF8.GetBytes |> alg.ComputeHash |> base64UrlEncode) = signature

        {
            new IJwtAuthority with
                member this.IssueToken header payload = issue header payload
                member this.VerifyToken token = verify token
                member this.Dispose() = alg.Dispose()
        }


let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL211c2EtcHJvamVjdC5ldS5hdXRoMC5jb20vIiwic3ViIjoiYXV0aDB8NTk1MjYzYTVhM2U0Mzc2ZmViZDQwNmRkIiwiYXVkIjoiUVV2Z2Rhak53VEJYdzlaeWF5VFJLU2VsR2lpdWRvMEoiLCJleHAiOjE1MDIxNjYwODcsImlhdCI6MTUwMjEzMDA4N30.21SGYF71ZZpTYqe-MuR4kzCc0jmhPFXNGbJWaAZQzT4"

open System.Text
open System.Security.Cryptography

let encodedSecret = "g78UyD-CAHSLGI50YyPo4zGiYZYo-bbGUO_DE2T6YTGqgh_WMb9WQeRc5v0jkgAz" |> System.Text.Encoding.UTF8.GetBytes
let testAuth = JWT.newJwtAuthority (fun key -> new HMACSHA256(key) :> HMAC) encodedSecret

testAuth.VerifyToken token


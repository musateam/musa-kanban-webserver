namespace Musa.Framework

open System.Text

module ServiceSelection = 

    let getBySessionId sessionId = 
        let db = Db.getMusaDb
        query {
            for ss in db.Musa.CspServiceSelection do
            where (ss.SessionId = uint32 sessionId)
            headOrDefault
        }

    let getData sessionId = 
        let db = Db.getMusaDb
        let row = getBySessionId sessionId
        row.Data

    let create (sessionId, request: Suave.Http.HttpRequest) = 
        let db = Db.getMusaDb
        let newServiceSelection = db.Musa.CspServiceSelection.Create()
        newServiceSelection.SessionId <- sessionId
        newServiceSelection.Data <- Encoding.UTF8.GetString(request.rawForm)
        db.SubmitUpdates()
        newServiceSelection.Data

    let update (sessionId, request:Suave.Http.HttpRequest) = 
        let db = Db.getMusaDb
        let q = getBySessionId sessionId
        match q with
        | null -> create (sessionId, request)
        | _ -> q.Data <- Encoding.UTF8.GetString(request.rawForm)
               db.SubmitUpdates()
               q.Data
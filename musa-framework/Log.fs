namespace Musa.Framework


module Log = 

    open System.IO
    open System.Text
    open System

    let logPath = Path.Combine(Directory.GetCurrentDirectory(), "Logs//")

    let write toFile (data:string) = 
        let timedate = DateTime.UtcNow
        let stringToWrite = "==== " + timedate.ToString("yyyy-MM-dd HH:mm:ss:ffff") + " ====" + Environment.NewLine
                            + data
                            + Environment.NewLine
        File.AppendAllText(logPath + toFile + ".log", stringToWrite)

    let writeRiskLog (request:Suave.Http.HttpRequest) =
        let riskWriter = write "riskAssessment"
        Encoding.UTF8.GetString(request.rawForm) |> riskWriter
        "OK"

    let writeOtherLog (request:Suave.Http.HttpRequest) =
        let otherWriter = write "other"
        Encoding.UTF8.GetString(request.rawForm) |> otherWriter
        "OK"
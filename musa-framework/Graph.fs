namespace Musa.Framework

module Graph = 
    open System.Text
    
    // let runGraphQuery (query:string) =
    //     let session = Db.runGraphDBQuery
    //     session.Run(query)

    let exploreGraph (request:Suave.Http.HttpRequest) = 
        Encoding.UTF8.GetString(request.rawForm) |> Db.runGraphDBQuery 
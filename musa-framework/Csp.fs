namespace Musa.Framework

open Newtonsoft.Json

module Csp = 

    type Provider = {
        Name: string;
        ProviderId: int32;
        ImageUrl: string;
        Location: string;
    }

    type Metric = {
        Id: uint32;
        SlaId: string;
        Question: string;
        Answer: string;
        Value: sbyte;
        ProviderId: uint32;
    }

    type CspsData = {Name: string; Provider: Provider; Metrics: Metric[]}

    let getAll (request)  = 
        let db = Db.getMusaDb
        query { 
            for csps in db.Musa.CspProviders do
            select csps
        } |> Seq.map(fun v -> 
                {
                    Name = v.Name;
                    Provider = v.MapTo<Provider>()
                    Metrics =
                        query {
                            for metrics in db.Musa.CspMetrics do
                            where (metrics.ProviderId = v.ProviderId)
                            select metrics
                        } |> Seq.map(fun m -> // TODO: convert that to .MapTo encapsulation
                            {
                                Id = m.Id;
                                SlaId = m.SlaId;
                                Question = m.Question;
                                Answer = m.Answer;
                                Value = m.Value;
                                ProviderId = m.ProviderId;
                            }) |> Seq.toArray
                }
        )
    
    let get providerId = 
        let db = Db.getMusaDb
        query {
            for csps in db.Musa.CspProviders do
            where (csps.ProviderId = providerId)
            headOrDefault
        } 

    let getMetrics providerId = 
        let db = Db.getMusaDb
        query {
            for metics in db.Musa.CspMetrics do 
            where (metics.ProviderId = providerId)
            headOrDefault
        }

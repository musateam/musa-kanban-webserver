namespace Musa.Framework

open Hekate
open FSharp.Data
open System

module Nist2CCM = 

    type Node = string * string
    type Edge = string * string * string
    type NodePair = Node * Node list

    type INodes = Node list
    type IEdges = Edge list
    type INodePairs = NodePair seq

    let loadCCM2NistFile (path:string) =
        CsvFile.Load(path, ";").Cache()

    let generateNode (payload:string) :Node = 
        (payload, payload)

    let generateNodes (ccm2NistFile:Runtime.CsvFile<CsvRow>) =
        ccm2NistFile.Rows
        |> Seq.map(fun row -> 
            let ccmNode:Node = (row.GetColumn "CCM") |> generateNode
            let nistNodes:INodes = (row.GetColumn "NIST").Split [|','|] 
                                |> Array.map generateNode
                                |> Array.toList
            (ccmNode,nistNodes))

    let getNodes (nodesTuple:INodePairs) :INodes = 
        nodesTuple 
        |> Seq.map(fun nP -> fst nP::snd nP)
        |> Seq.toList
        |> List.concat
            
    let getEdges (nodesTuple:INodePairs) :IEdges =
        nodesTuple 
        |> Seq.map(fun (ccmNode:Node, nistNodes:INodes) -> 
                    nistNodes 
                    |> List.map(fun nN ->
                        (fst nN, fst ccmNode, "right")))
        |> Seq.toList
        |> List.concat

    let generateGraph (ccm2NistFilePath:string) = 
        let nodes = ccm2NistFilePath 
                    |> loadCCM2NistFile
                    |> generateNodes
        
        Graph.create 
            (nodes |> getNodes)
            (nodes |> getEdges)

    let findNodeGuid graph (id:string) = 
        Graph.Nodes.find id graph
        |> snd

    let getPayloadOfNode (graph:Graph<string,string,string>) (payload:string) = 
        Graph.Nodes.toList graph
        |> List.find(fun v -> snd v = payload)
        |> fst

    let getCcmWithNist (graph:Graph<string, string, string>) (payload:string) = 
        try
            let nodeGuid = getPayloadOfNode graph payload 
            let ccmControls = Graph.Nodes.neighbours nodeGuid graph
            match ccmControls with
            | Some l -> l |> List.map(fun edge -> 
                                        let (id, direction) = edge
                                        getPayloadOfNode graph id)
            | None -> List.empty 
        with
        | exn as ex -> ["nok"]

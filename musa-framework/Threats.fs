namespace Musa.Framework 

module Threats = 

    type OwaspData = {
        skillLevel: uint32;
        motive: uint32;
        opportunity: uint32;
        size: uint32;
        easeOfDiscovery: uint32;
        easeOfExploit: uint32;
        awareness: uint32;
        intrusionDetection: uint32;
        lossOfConfidentiality: uint32;
        lossOfIntegrity: uint32;
        lossOfAvailability: uint32;
        lossOfAccountability: uint32;
    }

    type Threat = {
        id: uint64;
        threatId: string;
        name: string;
        description: string;
        strideId: uint64;
        strideName: string;
        sR: seq<string>;
        owasp: OwaspData;
    }

    type ThreatStride = {
        id: uint32;
        threatId: uint32;
        strideId: uint32;
    }

    type Stride = {
        id: uint64;
        name: string;
    }

    let getAll =
        let db = Db.getSlaGeneratorDb
        query {
            for t in db.Slagenerator.Threats do
            join c in db.Slagenerator.ThreatsStrides on (t.Id = c.ThreatId)
            join s in db.Slagenerator.Strides on (c.StridesId = s.Id)
            select {
                id = t.Id;
                threatId = Db.StringFromOption t.ThreatId;
                name = Db.StringFromOption t.ThreatName;
                description = Db.StringFromOption t.ThreatDescription;
                strideId = s.Id;
                strideName = Db.StringFromOption s.Name;
                sR = Seq.empty;
                owasp = {
                        skillLevel = Db.Uint32FromOption t.SkillLevel;
                        motive = Db.Uint32FromOption t.Motive;
                        opportunity = Db.Uint32FromOption t.Opportunity;
                        size = Db.Uint32FromOption t.Size;
                        easeOfDiscovery = Db.Uint32FromOption t.EaseOfDiscovery;
                        easeOfExploit = Db.Uint32FromOption t.EaseOfExploit;
                        awareness = Db.Uint32FromOption t.Awareness;
                        intrusionDetection = Db.Uint32FromOption t.IntrusionDetection;
                        lossOfConfidentiality = Db.Uint32FromOption t.LossOfConfidentiality;
                        lossOfIntegrity = Db.Uint32FromOption t.LossOfIntegrity;
                        lossOfAvailability = Db.Uint32FromOption t.LossOfAvailability;
                        lossOfAccountability = Db.Uint32FromOption t.LossOfAccountability;
                };
            }
        }

    let getAllStride = 
        let db = Db.getSlaGeneratorDb
        query {
            for s in db.Slagenerator.Strides do
            select {
                id = s.Id;
                name = Db.StringFromOption s.Name;
            }
        } 


namespace Musa.Framework

open System.Text
open FSharp.Data

module RiskProfile = 
    type RiskProfileSchema = JsonProvider<"""../riskProfileExample.json""">

    let getBySessionId sessionId = 
        let db = Db.getMusaDb
        query {
            for r in db.Musa.RisRiskProfile do
            where (r.SessionId = uint32 sessionId)
            headOrDefault
        }
    
    let getByComponentId (sessionId, componentId) = 
        let db = Db.getMusaDb
        query {
            for r in db.Musa.RisRiskProfile do
            where (r.SessionId = sessionId)
            where (r.ComponentId = Some componentId)
            headOrDefault
        } 

    let getSlaRiskProfile (sessionId, componentId) = 
        let q = getByComponentId (sessionId, componentId)
        match q with 
        | null -> "nok"
        | _ -> q.Data

    let getData sessionId =
        let db = Db.getMusaDb
        let row = getBySessionId sessionId
        row.Data

    let update (sessionId, request:Suave.Http.HttpRequest) = 
        let db = Db.MusaSql.GetDataContext(Db.MusaConnectionString)
        let q = 
            query {
                for r in db.Musa.RisRiskProfile do
                where (r.SessionId = uint32 sessionId)
                headOrDefault
            }
        match q with
        | null -> "nok"
        | _ ->  q.Data <- Encoding.UTF8.GetString(request.rawForm)
                db.SubmitUpdates()
                q.Data

    let updateComponent(sessionId, componentId, request:Suave.Http.HttpRequest) = 
        let db = Db.MusaSql.GetDataContext(Db.MusaConnectionString)
        let q = 
            query {
                for r in db.Musa.RisRiskProfile do
                where (r.SessionId = sessionId)
                where (r.ComponentId = Some componentId)
                headOrDefault
            } 
        match q with
        | null -> let newRiskProfile = db.Musa.RisRiskProfile.Create()
                  newRiskProfile.SessionId <- sessionId
                  newRiskProfile.ComponentId <- Some componentId
                  newRiskProfile.Data <- Encoding.UTF8.GetString(request.rawForm)
                  db.SubmitUpdates()
                  newRiskProfile.Data
        | _ -> q.Data <- Encoding.UTF8.GetString(request.rawForm)
               db.SubmitUpdates()
               q.Data

    let create (sessionId, request: Suave.Http.HttpRequest) = 
        let db = Db.MusaSql.GetDataContext(Db.MusaConnectionString)
        let newRiskProfile = db.Musa.RisRiskProfile.Create()
        newRiskProfile.SessionId <- sessionId
        newRiskProfile.Data <- Encoding.UTF8.GetString(request.rawForm)
        db.SubmitUpdates()
        newRiskProfile.Data

namespace Musa.Framework

open MySql.Data
open FSharp.Data.Sql
open FSharp.Configuration
open Neo4j.Driver.V1
open System

module Db =

    type Settings = AppSettings<"App.config">

    [<Literal>]
    let ResPath = __SOURCE_DIRECTORY__ + @"../packages/MySql.Data/lib/net45"

    type MusaSql = SqlDataProvider<DatabaseVendor = Common.DatabaseProviderTypes.MYSQL,
                                    ConnectionString = "Server=localhost;Database=musa;User=root;Password=root",
                                    ResolutionPath = ResPath,
                                    IndividualsAmount = 1000,
                                    UseOptionTypes = true,
                                    Owner = "musa">
    type SlaGenerator = SqlDataProvider<DatabaseVendor = Common.DatabaseProviderTypes.MYSQL,
                                        ConnectionString = "Server=localhost;Database=slagenerator;User=root;Password=root",
                                        ResolutionPath = ResPath,
                                        IndividualsAmount = 1000,
                                        UseOptionTypes = true,
                                        Owner = "slagenerator">
    let StringFromOption opt = 
        match opt with
        | Some opt -> opt
        | None -> ""

    let Uint32FromOption opt = 
        match opt with
        | Some opt -> opt
        | None -> uint32 0

    let MusaConnectionString = Settings.ConnectionStrings.MusaConnection

    let getMusaDb = MusaSql.GetDataContext(Settings.ConnectionStrings.MusaConnection)

    let getSlaGeneratorDb = SlaGenerator.GetDataContext(Settings.ConnectionStrings.SlaeditorConnection)


    let runGraphDBQuery (query:string) = 
        let authenticationToken = AuthTokens.Basic(Settings.GraphDbUser, Settings.GraphDbPassword)
        let driver = GraphDatabase.Driver(Settings.ConnectionStrings.GraphDbConnection, authenticationToken)
        let session = driver.Session() 
        session.Run(query)
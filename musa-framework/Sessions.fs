﻿namespace Musa.Framework

open MySql.Data
open System
open System.Data
open FSharp.Data.Sql
open FSharp.Data
open FSharp.Configuration
open Newtonsoft.Json


module Sessions = 

    let containsGroup (requestGroups:string) (dbGroups:string) = 
        let dbGroupsArray = dbGroups.Split [|','|]
        let requestGroupsArray = requestGroups.Split[|','|]
        dbGroupsArray |> Array.filter (fun dE -> requestGroupsArray
                                                |> Array.exists (fun rE -> dE = rE))
        |> Array.length > 0

    let getSessionById sessionId userId = 
        let db = Db.getMusaDb
        query {
            for session in db.Musa.ComSession do
            where (session.SessionId = uint32 sessionId)
            //where (session.UserId = userId)
            headOrDefault
        }

    let get sessionId request =
        let q = getSessionById sessionId (Http.getUserId request)
        match q with 
        | null -> "nok" 
        | _ -> q.Text


    let getAll (request) = 
        let db = Db.MusaSql.GetDataContext(Db.MusaConnectionString)
        let requestGroups = Http.getGroups request
        let sessions = query {
                for session in db.Musa.ComSession do
                select session
            } 
        sessions
        |> Seq.filter (fun s -> containsGroup requestGroups s.Groups)
        |> Seq.map (fun s -> s.SessionId)

    let create (request:Suave.Http.HttpRequest) = 
        let db = Db.MusaSql.GetDataContext(Db.MusaConnectionString)
        let row = db.Musa.ComSession.Create()
        row.Text <- System.Text.Encoding.UTF8.GetString(request.rawForm)
        row.UserId <- Http.getUserId request
        row.Groups <- Http.getGroups request
        db.SubmitUpdates()
        row.SessionId

    let update (sessionId, request:Suave.Http.HttpRequest) = 
        let db = Db.MusaSql.GetDataContext(Db.MusaConnectionString)
        let q = 
            query {
                for session in db.Musa.ComSession do
                where (session.SessionId = uint32 sessionId)
                // where (session.UserId = (Http.getUserId request))
                headOrDefault
            }
        match q with 
        | null -> string (create (request))
        | _ -> q.Text <- System.Text.Encoding.UTF8.GetString(request.rawForm)
               db.SubmitUpdates()
               q.Text


    let delete sessionId request =
        let db = Db.getMusaDb
        let q = getSessionById sessionId (Http.getUserId request)
        match q with 
        | null -> "nok"
        | _ -> q.Delete()
               db.SubmitUpdates()
               "ok"
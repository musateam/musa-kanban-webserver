namespace Musa.Framework

open Suave
open Suave.Http
open Suave.Operators
open Suave.Filters
open Suave.Successful
open Suave.Files
open Suave.RequestErrors
open Suave.Logging
open Suave.Utils
open System
open System.Net
open Suave.Sockets
open Suave.Sockets.Control
open Suave.WebSocket
open System.IO
open Newtonsoft.Json
open FSharp.Configuration

module Main =
    type Settings = AppSettings<"App.config">
    let publicFolder = Path.Combine(Directory.GetCurrentDirectory(), "webfiles")

    let port = Settings.Port 
    let ip = Settings.Address

    let serverConfig = {
        defaultConfig with
            homeFolder = Some(publicFolder)
            bindings = [ HttpBinding.createSimple HTTP ip port ]
            hideHeader = true
    }

    let ccmNistGraph = Nist2CCM.generateGraph Settings.CcmNistCsv
    let translsateNist2CCM = Nist2CCM.getCcmWithNist ccmNistGraph

    let app = 
        choose 
            [
                GET >=> choose
                    [
                        path "/" >=> Redirection.redirect "index.html"; Files.browseHome
                        path "/session" >=> request (fun r -> Http.processWithAuthentication (Sessions.getAll r |> Http.toJson) r) >=> Http.mimeJson 
                        pathScan "/session/%d" (fun attr -> request (fun r -> (Sessions.get attr r, r) ||> Http.processWithAuthentication)) >=> Http.mimeJson
                        pathScan "/riskprofile/%d" (fun attr -> request (fun r -> (RiskProfile.getData attr, r) ||> Http.processWithAuthentication)) >=> Http.mimeJson
                        pathScan "/riskprofile/%d/component/%s" (fun attr -> request (fun r -> (RiskProfile.getSlaRiskProfile attr, r) ||> Http.processWithAuthentication)) >=> Http.mimeJson
                        path "/csp" >=> request (fun r -> (Csp.getAll r |> Http.toJson, r) ||> Http.processWithAuthentication) >=> Http.mimeJson
                        pathScan "/serviceselection/%d" (fun attr -> request (fun r -> (ServiceSelection.getData attr, r) ||> Http.processWithAuthentication)) >=> Http.mimeJson
                        pathScan "/nist/%s/ccm" (fun attr -> request (fun r -> (translsateNist2CCM attr |> Http.toJson, r) ||> Http.processWithAuthentication)) >=> Http.mimeJson
                        pathScan "/slaeditor/threat/%d/controls" (fun attr -> request (fun r -> (Controls.getControlsForThreat attr |> Http.toJson, r) ||> Http.processWithAuthentication)) >=> Http.mimeJson
                        path "/slaeditor/threat" >=> request (fun r -> (Threats.getAll |> Http.toJson, r) ||> Http.processWithAuthentication) >=> Http.mimeJson
                        path "/slaeditor/stride" >=> request (fun r -> (Threats.getAllStride |> Http.toJson, r) ||> Http.processWithAuthentication) >=> Http.mimeJson
                        path "/slaeditor/control" >=> request (fun r -> (Controls.getAll |> Http.toJson, r) ||> Http.processWithAuthentication) >=> Http.mimeJson
                    ]
                POST >=> choose
                    [
                        path "/session" >=> request (fun r -> (Sessions.create (r) |> Http.toJson |> CREATED))
                        pathScan "/riskprofile/%d" (fun sessionId -> request (fun r -> (((sessionId, r) |> RiskProfile.create),  r) ||> Http.processWithAuthentication))
                    ]
                PUT >=> choose
                    [
                        pathScan "/session/%d" (fun sessionId -> request (fun r -> (Sessions.update(sessionId, r), r) ||> Http.processWithAuthentication ))
                        pathScan "/riskprofile/%d" (fun sessionId -> request (fun r -> (RiskProfile.update(sessionId, r), r) ||> Http.processWithAuthentication))
                        path "/riskprofile/log" >=> request (fun r -> (Log.writeRiskLog r, r) ||> Http.processWithAuthentication)
                        pathScan "/riskprofile/%d/component/%s" (fun (sessionId, componentId) -> request (fun r -> (RiskProfile.updateComponent(sessionId, componentId, r), r) ||> Http.processWithAuthentication))
                        pathScan "/serviceselection/%d" (fun sessionId -> request (fun r -> (ServiceSelection.update(sessionId, r), r) ||> Http.processWithAuthentication))
                        path "/graph/query" >=>  request (fun r -> (Graph.exploreGraph r |> Http.toJson, r) ||> Http.processWithAuthentication ) >=> Http.mimeJson
                        path "/other/log" >=> request (fun r -> (Log.writeOtherLog r, r) ||> Http.processWithAuthentication)
                    ]
                DELETE >=> choose
                    [
                        pathScan "/session/%d" (fun attr -> request (fun r -> (Sessions.delete attr r, r) ||> Http.processWithAuthentication))
                    ]
                NOT_FOUND "Found no way to process it"
            ]

    [<EntryPoint>]
    let main _ = 
        startWebServer serverConfig app 
        0
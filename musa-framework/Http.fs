namespace Musa.Framework

module Http = 

    open Suave
    open Suave.Operators
    open Newtonsoft.Json
    open Suave.Successful
    open Suave.RequestErrors

    let createResponse res = match res with 
                             | "nok" | null | "[\"nok\"]" -> NOT_FOUND "Not found"
                             | _ -> OK res
    
    let toJson object = 
        JsonConvert.SerializeObject (object) 

    let mimeJson = 
        Writers.setMimeType "application/json"

    let processWithAuthentication successWork (request:Suave.Http.HttpRequest) = 
        match request.header "Authorization" with
        | Choice1Of2 t -> match Token.validate t with
                          | true -> successWork |> createResponse 
                          | false -> FORBIDDEN "Token not valid"
        | Choice2Of2 err -> UNAUTHORIZED "Token not specified" 

    let processRequestAuth successWork = 
        request (fun r -> 
           processWithAuthentication successWork r)

    let getUserId (request:Suave.Http.HttpRequest) = 
        match request.header "UserId" with
        | Choice1Of2 userId -> userId
        | Choice2Of2 err -> "auth1"


    let getGroups (request:Suave.Http.HttpRequest) = 
        match request.header "Groups" with
        | Choice1Of2 groups -> groups
        | Choice2Of2 err -> ""

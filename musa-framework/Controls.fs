namespace Musa.Framework

module Controls = 

    type Control = {
        id: string;
        name: string;
        family: string;
        familyId: string;
        description: string;
    }

    let getAll = 
        let db = Db.getSlaGeneratorDb
        query {
            for c in db.Slagenerator.Controls do
            select {
                id = Db.StringFromOption c.ControlId
                name = Db.StringFromOption c.ControlName 
                family = Db.StringFromOption c.FamilyName 
                familyId = Db.StringFromOption c.FamilyId
                description = Db.StringFromOption c.ControlDescription
            }
        }

    let getControlsForThreat threatId = 
        let db = Db.getSlaGeneratorDb
        query {
            for c in db.Slagenerator.Controls do
            join t in db.Slagenerator.ThreatsSuggestedControls on (c.Id = t.SuggestedControlsId)
            where (t.ThreatId = uint64 threatId)
            select {
                id = Db.StringFromOption c.ControlId                  
                name = Db.StringFromOption c.ControlName  
                family = Db.StringFromOption c.FamilyName                  
                familyId = Db.StringFromOption c.FamilyId                  
                description = Db.StringFromOption c.ControlDescription             
            }
        } 
        
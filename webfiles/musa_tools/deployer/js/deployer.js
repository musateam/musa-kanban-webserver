/**
 * Deployer generic utils
 * Usage:
 *  const deployer=window.createDeployer()
 *  deployer.serviceSession.getLocalJSON()
 */
window.createDeployer = function () {

    const LITERAL_MUSA_SESSION = 'musa.session'
    const LITERAL_MUSA_TOKEN = 'musa.sso.token'

    /**
     * Manage localStorage and Remote session service
     */
    class ServiceSession {
        constructor() {
            this.id = getQueryStringParameter('appId')
            let root = window.location.origin.replace('deployer', 'framework')
            // if (window.location.host.includes('localhost')) root = ...
            this.url = root + '/session/' + this.id
        }
        getLocal() {
            return localStorage[LITERAL_MUSA_SESSION]
        }
        getLocalJSON() {
            return JSON.parse(this.getLocal())
        }
        putLocal(json) {
            if (!json) throw new Error('Invalid session: ' + this.id)
            localStorage[LITERAL_MUSA_SESSION] = JSON.stringify(json)
        }
        getRemote() {
            return new Promise(function (resolve, reject) {
                try {
                    let musa_session = JSON.parse(localStorage[LITERAL_MUSA_SESSION]);
                    resolve(musa_session)
                } catch (err) { reject('Error when retrieving session from server: ' + err.status + ':' + err.statusText) }
            })
        }
        putRemote() {
            const self = this
            return new Promise(function (resolve, reject) {
                let data = localStorage[LITERAL_MUSA_SESSION]
                if (!data) reject(new Error('musa.session not found in localStorage'))
                $.ajax({ url: self.url, method: "PUT", dataType: 'json', data: data, headers: deployer.securityHeaders() })
                    .done(res => resolve())
                    .fail(err => reject('Error when updating session in server: ' + err.status + ': ' + err.statusText))
            })
        }
    }

    function getQueryStringParameter(key) {
        const params = (new URL(document.location)).searchParams
        const value = params.get(key)
        if (!value) alert(`Error: the URL must contain the "${key}" parameter`)
        return value
    }

    /**
     * Deployer utils Object
     * This is the object that a developer shoud use after invoking window.createDeployer()
     * @type {Object}
     */
    const deployer = {
        id: getQueryStringParameter('appId'),
        name: getQueryStringParameter('appName'),
        serviceSession: new ServiceSession(),
        get url() { return window.location.origin },
        get securityHeaders() {
            const token = localStorage.getItem(LITERAL_MUSA_TOKEN)
            if (token) return { "X-access-token": token }
            else return {}
        }
    }

    return deployer
}
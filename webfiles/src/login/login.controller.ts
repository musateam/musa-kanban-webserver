/// <reference path="../../typings/index.d.ts" />

module MUSA.Controllers {
    "use strict";

    class Login {
        constructor(private lock) {

        }
    }

    angular.module('musa').controller('LoginController', Login);
}
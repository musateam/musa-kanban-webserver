/// <reference path="../typings/index.d.ts" />

module MUSA {
    "use strict";

    angular.module('musa',
        [
            'ng'
            , 'ui.router'
            , 'xeditable'
            , 'LocalStorageModule'
            , 'as.sortable'
            , 'ui.bootstrap'
            , 'd3-multi-parent'
            , 'ui.bootstrap.contextMenu'
            , 'ui.select'
            , 'ngSanitize'
            , 'toggle-switch'
            , 'rzModule'
            , 'auth0.lock'
            , 'angular-jwt'
        ]);
}


/// <reference path="../../typings/index.d.ts" />

module MUSA.Constants {
    "use strict";

    class HostConfig implements IConfig {
        public modellerHost;
        public riskHost;
        public dstHost;
        public deployerHost;
        public slaGeneratorHost;
        public monitorHost;
        public dashboardHost;
        constructor(
        ) {
            this.dashboardHost = 'http://framework.musa-project.eu/';
            this.riskHost = this.dashboardHost;
            this.dstHost = this.dashboardHost;
            this.deployerHost = this.dashboardHost + 'deployer/';
            this.slaGeneratorHost = this.dashboardHost + 'sla-generator/';
            this.monitorHost = 'http://assurance-platform.musa-project.eu/';
            this.modellerHost = this.dashboardHost + 'modeller/';

            if (window.location.host.includes("localhost")) {
                // for dev purposes
                this.dashboardHost = 'http://localhost:8083/';
            } else if (window.location.host.includes("dev")) {
                // dev deployment   
                this.deployerHost = this.replaceHost(this.deployerHost, 'framework');
                this.slaGeneratorHost = this.replaceHost(this.slaGeneratorHost, 'framework');
                this.modellerHost = this.replaceHost(this.modellerHost, 'framework');
            }
        }

        replaceHost(input, word) {
            return input.replace(word, `${word}-dev`);
        }
    }

    angular.module('musa').constant('HostConfig', new HostConfig());
}
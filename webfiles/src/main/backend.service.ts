/// <reference path="../../typings/index.d.ts" />

module MUSA.Backend {
    "use strict";

    class Backend implements IBackend {
        private API: string;
        private AuthToken: string;
        private config: ng.IRequestShortcutConfig;

        constructor(private $http: ng.IHttpService, private BackendHost: MUSA.IUrl, private route: string, private localStorageService) {
            this.API = BackendHost.path() + route;
            this.AuthToken = this.localStorageService.get('sso.token') || "noToken";

            this.config = {
                headers: {
                    'Authorization': this.AuthToken
                    , 'UserId': (_.isNull(this.localStorageService.get('sso.profile'))) ? {} : this.localStorageService.get('sso.profile')["user_id"]
                    , 'Groups': (_.isNull(this.localStorageService.get('sso.profile'))) ? {} : this.localStorageService.get('sso.profile')["groups"]
                }
            }

        }

        url() {
            return this.API;
        }

        fetch(endPoint) {
            return this.$http.get(this.API + endPoint, this.config);
        }

        insert(endPoint, data) {
            return this.$http.post(this.API + endPoint, data, this.config);
        }

        update(endPoint, data) {
            return this.$http.put(this.API + endPoint, data, this.config);
        }

        remove(endPoint) {
            return this.$http.delete(this.API + endPoint, this.config);
        }

        fetchExt(endPoint) {
            return this.$http.get(endPoint, this.config);
        }

        insertExt(endPoint, data) {
            return this.$http.post(endPoint, data, this.config);
        }

        updateExt(endPoint, data) {
            return this.$http.put(endPoint, data, this.config);
        }

        removeExt(endPoint) {
            return this.$http.delete(endPoint, this.config);
        }
    }

    angular.module('musa').service('Backend', ($http, BackendHost, localStorageService) => {
        return new Backend($http, BackendHost, '', localStorageService);
    });
}
/// <reference path="../../typings/index.d.ts" />

module MUSA.Auth {
    "use strict";

    class Auth implements IAuth {
        public token: string;
        public profile: string;

        constructor(private lock, public localStorageService, public jwtHelper) {

            this.token = this.localStorageService.get('sso.token') || "";
            this.profile = this.localStorageService.get('sso.profile') || "";

            // Listen to authenticated method
            this.lock.on('authenticated', (authResult) => {
                console.log('auth', authResult);
                this.token = authResult.idToken;
                this.localStorageService.set('sso.token', this.token);

                this.lock.getProfile(authResult.idToken, (err, profile) => {
                    if (err) {
                        console.log('error Auth0', err);
                    }

                    this.profile = profile;
                    console.log('profile', this.profile);
                    this.localStorageService.set('sso.profile', this.profile);
                    window.location.reload();
                });
            });

        }

        loginShow() {
            return this.lock.show();
        }

        logout() {
            this.lock.logout();
            this.localStorageService.remove('sso.token');
            this.localStorageService.remove('sso.profile');
            window.location.reload();
        }

        isLoggedIn() {
            return (this.token != "" && !this.jwtHelper.isTokenExpired(this.token));

        }
    }

    angular.module('musa').service('Auth', (lock, localStorageService, jwtHelper) => {
        return new Auth(lock, localStorageService, jwtHelper);
    })
}
/// <reference path="../../typings/index.d.ts" />

module MUSA.Constants {
    "use strict";


    class Url implements IUrl {
        public host;
        public port;
        public protocol;
        constructor(host, port = 80, protocol = 'http') {
            this.host = host;
            this.port = port;
            this.protocol = protocol;
        }

        path() {
            return `${this.protocol}://${this.host}:${this.port}/`
        }
    }

    angular.module('musa').constant('BackendHost', new Url(window.location.hostname, _.isEmpty(window.location.port) ? 80 : parseInt(window.location.port)));
}
/// <reference path="../../typings/index.d.ts" />


module MUSA.Config {

    class Config {
        constructor(private localStorageServiceProvider
            , private $stateProvider: ng.ui.IStateProvider
            , private $urlRouterProvider: ng.ui.IUrlRouterProvider
            , private lockProvider
            , private $sceDelegateProvider: ng.ISCEDelegateProvider) {
            this.init();

            this.lockProvider.init({
                clientID: 'QUvgdajNwTBXw9ZyayTRKSelGiiudo0J'
                , domain: 'musa-project.eu.auth0.com'
                , options: {
                    _idTokenVerification: false
                    , auth: {
                        responseType: "token"
                        , redirect: false
                        , params: {
                            scope: 'openid email app_metadata'
                        }
                    }
                }
            });

        }

        private init(): void {
            this.$stateProvider.state('component', Config.component());
            this.$stateProvider.state('riskprofile', Config.riskprofile());
            this.$stateProvider.state('login', Config.login());
            this.$urlRouterProvider.otherwise('/component');

            this.localStorageServiceProvider.setPrefix('musa');
            this.$sceDelegateProvider.resourceUrlWhitelist(['http://*.musa-project.eu/**', 'self']);
        }

        private static component(): ng.ui.IState {
            return {
                url: '/component'
                , controller: 'ComponentController as k'
                , templateUrl: 'src/component/template/component.html'
            }
        }

        private static login(): ng.ui.IState {
            return {
                url: '/login'
                , controller: 'LoginController as l'
                , templateUrl: 'src/login/template/login.html'
            }
        }

        private static riskprofile(): ng.ui.IState {
            return {
                url: '/riskprofile'
                , controller: 'RiskController as r'
                , templateUrl: 'src/riskProfile/template/riskprofile.html'
            }
        }
    }

    class Run {
        constructor(private editableOptions, private editableThemes, private lock) {
            this.editableOptions.theme = 'bs3';
            this.editableThemes.buttonClass = 'btn-xs';

            this.lock.interceptHash();

            // this.lock.on('authenticated', (authResult) => {
            //     console.log('auth', authResult);
            //     localStorage.setItem('id_token', authResult.idToken);

            //     this.lock.getProfile(authResult.idToken, (err, profile) => {
            //         if (err) {
            //             console.log('error Auth0', err);
            //         }

            //         localStorage.setItem('profile', JSON.stringify(profile));
            //     });
            // });
        }

    }


    angular.module('musa').config(
        (localStorageServiceProvider
            , $stateProvider: ng.ui.IStateProvider
            , $urlRouterProvider: ng.ui.IUrlRouterProvider
            , lockProvider
            , $sceDelegateProvider) => {
            new Config(localStorageServiceProvider, $stateProvider, $urlRouterProvider, lockProvider, $sceDelegateProvider)
        })
        .run((editableOptions, editableThemes, lock) => new Run(editableOptions, editableThemes, lock));

}

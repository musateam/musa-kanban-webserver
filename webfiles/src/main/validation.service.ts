/// <reference path="../../typings/index.d.ts" />

module MUSA.Services {

    class Validation implements IValidationService {

        private sessionData: object;

        constructor(private Session: MUSA.ISessionService
            , private Backend: MUSA.IBackend
            , private HostConfig: MUSA.IConfig
            , private localStorageService
            , private MACM: MUSA.IMacm) {

            this.sessionData = this.Session.get();

            // check monitoring every minute
            setInterval(this.validateMonitor(), 60 * 1000);
        }

        updateItemsValidation(key, value, itemToUpdate = "all") {
            _.each(this.sessionData["columns"], (column, idx) => {
                _.each(column["items"], (item, iidx) => {
                    if (_.isUndefined(this.sessionData["columns"][idx]["items"][iidx]["statusOK"])) {
                        this.sessionData["columns"][idx]["items"][iidx]["statusOK"] = [];
                    }
                    if (itemToUpdate != "all" && item["cid"] == itemToUpdate["cid"]) {
                        this.sessionData["columns"][idx]["items"][iidx]["statusOK"][key] = value;
                    } else {
                        this.sessionData["columns"][idx]["items"][iidx]["statusOK"][key] = value;
                    }
                });
            });
        };

        updateMachineData() {
            _.each(this.sessionData["columns"], (column, idx) => {
                _.each(column["items"], (item, iidx) => {
                    if (_.isEmpty(item["deploymentMachine"])) {
                        this.MACM.runGraphQuery(
                            this.MACM.getVMForComponentQuery(this.sessionData["applicationId"], item["cid"])
                        ).then(r =>
                            this.sessionData["columns"][idx]["items"][iidx]["deploymentMachine"] = (!!r["data"][0]) ? r["data"][0]["Values"]["s"]["Properties"] : {})
                    }
                });
            });
        }

        validateModel() {
            var url = this.HostConfig.modellerHost + 'eu.musa.modeller.ws/webresources/camelfilews/getValidationErrorsByApplication/' + this.sessionData["applicationId"];
            this.Backend.fetchExt(url).then(r => {
                this.updateItemsValidation("Model", (!!r.data["ValidationErrorStatus"]) ? r.data["ValidationErrorStatus"] : true);
            });
        }

        validateDeployer() {
            this.updateItemsValidation("Plan deployment", true);
            this.updateItemsValidation("Deploy", true);
        }

        validateServiceSellection() {
            this.Backend.fetchExt(this.sessionData["getDstUrl"] || this.HostConfig.dstHost + 'serviceselection/' + this.sessionData["applicationId"])
                .then(r => this.updateItemsValidation("Service Selection", true))
                .catch(err => this.updateItemsValidation("Service Selection", false));
        }

        validateMonitor() {
            console.log('monitoring refreshing');
            _.each(this.sessionData["columns"], (column, idx) => {
                _.each(column["items"], (item, iidx) => {
                    var url = this.HostConfig.monitorHost + "operator/status?appId="
                        + this.sessionData["applicationId"] + "&componentId="
                        + item["componentId"] + "&sso="
                        + this.localStorageService.get('sso.token');
                    this.Backend.fetchExt(url).then(r => {
                        if (!!r.data["status"]) {
                            if (r.data["underRemediation"] != 0 || r.data["alert"] != 0) {
                                this.updateItemsValidation("Monitor", "warning", item);
                            }
                            if (r.data["violate"] != 0) {
                                this.updateItemsValidation("Monitor", false, item);
                            }
                        }
                    });
                });
            });
        }

        validationMessage(columnName) {
            switch (columnName) {
                case "Model":
                    return "Model Validation errors";
                case "Risk":
                    return "Risk assessment incomplete";
                case "Service Selection":
                    return "Service selection incomplete";
                case "Plan Deployment":
                case "Deploy":
                    return "Deployment plan errors";
                case "Sla":
                case "SLA":
                    return "SLA generation errors";
                case "Monitor":
                    return "Some errors detected by the monitoring";
                default:
                    return "Step errors!";
            }
        }

        updateAllValidations() {
            this.validateModel();
            this.validateServiceSellection();
            this.validateDeployer();
            this.updateMachineData();
        }
    }

    angular.module('musa').service('Validation', (Session, Backend, HostConfig, localStorageService, MACM) => new Validation(Session, Backend, HostConfig, localStorageService, MACM))
}
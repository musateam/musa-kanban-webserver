/// <reference path="../../typings/index.d.ts" />

module MUSA.Services {

    class Session implements ISessionService {

        private sessionData: object;

        constructor(private localStorageService
            , private HostConfig: MUSA.IConfig
            , private Backend: MUSA.IBackend) {
            this.sessionData = (this.localStorageService.get('session') == null || this.localStorageService.get('session') === '' || !this.localStorageService.get('session')["applicationId"]) ?
                {
                    "applicationName": "New Application"
                    , "modelUrl": this.HostConfig.modellerHost
                    , "riskUrl": this.HostConfig.riskHost
                    , "dstUrl": this.HostConfig.dstHost
                    , "deployerUrl": this.HostConfig.deployerHost
                    , "columns": [
                        { "name": "Model", "items": [] }
                        , { "name": "Risk", "items": [] }
                        , { "name": "Service Selection", "items": [] }
                        , { "name": "SLA", "items": [] }
                        , { "name": "Plan Deployment", "items": [] }
                        , { "name": "Deploy", "items": [] }
                    ]
                }
                :
                this.localStorageService.get('session');


            if (!this.sessionData["applicationId"] || this.sessionData["applicationId"] == 0) {
                Backend.insert("session", this.sessionData).then((response) => {
                    this.sessionData["applicationId"] = response.data;
                    this.set(this.sessionData);
                });
            } else {
                this.refresh();
            }

        }

        get() {
            return this.sessionData;
        }

        set(newSessionData) {
            if (newSessionData != this.sessionData) {
                angular.copy(newSessionData, this.sessionData);
            }
            this.localStorageService.set('session', this.sessionData);
            this.Backend.update("session/" + this.sessionData["applicationId"], this.sessionData).then(response => console.log('updated session', response));
            return this.sessionData;
        }

        refresh() {
            this.Backend.fetch("session/" + this.sessionData["applicationId"]).then(r => {
                r.data["applicationId"] = this.sessionData["applicationId"];
                r.data["startMonitoringUrl"] = this.HostConfig.monitorHost + "?operator?start=1&appId=" + r.data["applicationId"] + "&sso=" + this.localStorageService.get('sso.token');
                angular.copy(r.data, this.sessionData);
                this.localStorageService.set('session', this.sessionData);
            });
            return this.sessionData;
        }
    }

    angular.module('musa').service('Session', (localStorageService, HostConfig, Backend) =>
        new Session(localStorageService, HostConfig, Backend));
}
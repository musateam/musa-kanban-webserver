/// <reference path="../../typings/index.d.ts" />

module MUSA.Directive {
    "use strict";

    declare var d3: any;

    function BarChartDirective() {

        function link(scope, element, attr) {
            // specify graph defaults with fallbacks
            var graph = {
                width: attr.width || 1000,
                height: attr.height || 900
            };

            var data = scope.data;

            // colouring function
            // var colors = d3.scale.category20();
            var colors = d3.scale.linear()
                .domain([0, 50, 100])
                .range(["#c0392b", "#f1c40f", "#27ae60"]);

            var chart = element[0],
                axisMargin = 20,
                margin = 20,
                valueMargin = 4,
                width = chart.offsetWidth,
                height = chart.offsetHeight,
                barHeight = (graph.height - axisMargin - margin * 2) * 0.4 / data.length,
                barPadding = (graph.height - axisMargin - margin * 2) * 0.6 / data.length,
                data, bar, svg, scale, xAxis, labelWidth = 0;

            var div = d3.select("body").append("div")
                .attr("class", "d3tooltip")
                .style("opacity", 0);

            svg = d3.select(chart)
                .append("svg")
                .attr("width", graph.width)
                .attr("height", graph.height);


            bar = svg.selectAll("g")
                .data(data)
                .enter()
                .append("g");

            bar.attr("class", "bar")
                .attr("cx", 0)
                .attr("transform", function (d, i) {
                    return "translate(" + margin + "," + (i * (barHeight + barPadding) + barPadding) + ")";
                });

            bar.append("text")
                .attr("class", "label")
                .attr("y", barHeight / 2)
                .attr("dy", ".35em") //vertical align middle
                .text(function (d) {
                    return d["SlaId"];
                }).each(function () {
                    labelWidth = Math.ceil(Math.max(labelWidth, this.getBBox().width));
                })
                .on("mouseover", function (d) {
                    div.transition()
                        .duration(200)
                        .style("opacity", .95);
                    div.html(d["description"] || "")
                        .style("left", (d3.event.pageX + 10) + "px")
                        .style("top", (d3.event.pageY + 10) + "px");
                })
                .on("mouseout", function (d) {
                    div.transition()
                        .duration(500)
                        .style("opacity", 0);
                });

            scale = d3.scale.linear()
                .domain([0, 100])
                .range([0, width - margin * 2 - labelWidth]);

            xAxis = d3.svg.axis()
                .scale(scale)
                .tickSize(-height + 2 * margin + axisMargin)
                .orient("bottom");

            bar.append("rect")
                .attr("transform", "translate(" + labelWidth + ", 0)")
                .attr("height", barHeight)
                .attr('fill', function (d) {
                    return colors(d["Value"] * 100);
                })
                .attr("width", function (d) {
                    return d["Value"] * 100;
                });

            bar.append("text")
                .attr("class", "value")
                .attr("y", barHeight / 2)
                .attr("dx", + labelWidth) //margin right
                .attr("dy", ".35em") //vertical align middle
                .attr("text-anchor", "end")
                .text(function (d) {
                    return Math.round(d["Value"] * 100).toString() + "%";
                })
                .attr("x", function (d) {
                    var width = this.getBBox().width;
                    var r = Math.max(width + valueMargin, d["Value"] * 100);
                    return (d["Value"] > 10) ? r + 25 : r;
                });
        }

        return {
            restrict: 'E'
            , scope: {
                data: '='
            }
            , link: link
        }
    }

    angular.module('musa').directive("barChart", BarChartDirective);
}
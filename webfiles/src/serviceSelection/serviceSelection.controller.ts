/// <reference path="../../typings/index.d.ts" />

module MUSA.Controllers {
    "use strict";

    class ServiceSelection {
        private deployments: Array<Object>;
        public components: Array<Object>;
        private selected: Array<Object>;
        public riskProfile: Array<String>;
        public session: Object;
        public vms: Object;
        public lastSelection: Object;
        public controls: Array<Object>;

        constructor(private localStorageService, private Backend: MUSA.IBackend, private HostConfig: MUSA.IConfig, private MACM: MUSA.IMacm, private Session: MUSA.ISessionService) {
            // Init
            this.deployments = [];
            this.selected = [];
            this.riskProfile = this.localStorageService.get('riskProfile') || [];
            this.session = this.Session.get();
            this.lastSelection = this.localStorageService.get('serviceSelection_' + this.session["applicationId"]) || {};
            this.controls = [];

            this.Backend.fetch('slaeditor/control').then(r => this.controls = r.data);

            // Fetch components
            this.components = _.flatten(
                _.map(
                    _.slice(this.localStorageService.get('session')["columns"], _.findIndex(this.localStorageService.get('session')["columns"], v => v["name"] == "Service Selection"))
                    , v => v["items"]));

            this.vms = _.groupBy(this.components, c => c["deploymentMachine"]["name"]);

            // Fetch Services
            this.Backend.fetch("csp").then((response) => {
                var services = response.data.map(v => {
                    var o = {};
                    o["data"] = v;

                    // generate alignment for the service
                    var allAlignment = this.calculateAlignment(v);
                    var selectedSR = this.getSelectedSecurityControlIds(this.riskProfile);

                    // var limitedAlignment = allAlignment.filter(v => {
                    //     return selectedSR.indexOf(v["SlaId"]) > -1
                    // });
                    o["ccmAlignment"] = allAlignment;
                    return o;
                });

                var possibleDeployments = [];

                _.forOwn(this.vms, (c, k) => {
                    var singleComponentDeployment = [];
                    _.each(services, s => {
                        singleComponentDeployment.push(
                            {
                                "component": c,
                                "service": s,
                                "vmName": k
                            }
                        )
                    });
                    possibleDeployments.push(singleComponentDeployment);
                });

                var permutations = this.cartesianProduct(possibleDeployments);

                // Calculate the scores
                this.deployments = permutations.map(singleDeployment => {
                    var o = {};
                    console.log(singleDeployment);
                    var deploymentValues = _(singleDeployment)
                        .map(singleCoponentOnSingleService => this.getSecurityControlForComponents(singleCoponentOnSingleService["service"]["ccmAlignment"], singleCoponentOnSingleService["component"]))
                        .flatten()
                        .map(values => values["Value"])
                        .value();
                    o["score"] = _.ceil(_(deploymentValues).sum() * 100 / deploymentValues.length, 2);
                    if (_.isNaN(o["score"])) {
                        o["score"] = 0;
                    }
                    o["data"] = singleDeployment;
                    return o;
                });
            });

        }

        cartesianProduct(arg) {
            var r = [], max = arg.length - 1;
            function helper(arr, i) {
                for (var j = 0, l = arg[i].length; j < l; j++) {
                    var a = arr.slice(0); // clone arr
                    a.push(arg[i][j])
                    if (i == max) {
                        r.push(a);
                    } else
                        helper(a, i + 1);
                }
            }
            helper([], 0);
            return r;
        }

        //
        calculateAlignment(service) {
            var groupAlignment =
                _(service["Metrics"])
                    .map(v => {
                        var i = {};
                        i["group"] = v["SlaId"].split(".")[0];
                        i["value"] = v["Value"];
                        return i
                    })
                    .groupBy("group")
                    .map((v, k) => {
                        var o = {};
                        o["SlaId"] = k;
                        o["Value"] = _.sumBy(v, "value") / v.length;
                        return o;
                    })
                    .value();
            _.each(groupAlignment, v => service["Metrics"].push(v));
            return service["Metrics"];
        }

        getSecurityControlsByComponent(riskProfile, key = 'ccm') {
            return _(riskProfile)
                .map(v => v['items'])
                .flatten()
                .map(r => {
                    return {
                        "cid": r['cid'],
                        "ccm": (!!r['risks']) ?
                            _(r['risks'])
                                .map(c => c[key])
                                .flatten()
                                .value()
                            :
                            []
                    }
                })
                .value();
        }

        getSecurityControlForComponents(allAlignment, componentsList) {
            return _(componentsList)
                .map(ss => this.getSecurityControlForComponent(allAlignment, ss["cid"]))
                .flatten()
                .value();
        }

        getSecurityControlForComponent(allAlignment, componentId) {
            var componentControls = _(this.getSecurityControlsByComponent(this.riskProfile))
                .find(v => v['cid'] == componentId);
            var componentControlMap = _(this.getSecurityControlsByComponent(this.riskProfile, 'ccmMap'))
                .find(v => v['cid'] == componentId);


            return (!_.isUndefined(componentControlMap) && !!componentControlMap["ccm"]) ?
                _(componentControlMap["ccm"])
                    .map(c => {
                        return (!!c) ? {
                            "SlaId": c["nist"],
                            "Value": _.ceil(_(allAlignment)
                                .filter(singleAlignment => c["ccm"].indexOf(singleAlignment["SlaId"]) > -1)
                                .sumBy(v => v["Value"])
                                / c["ccm"].length
                                , 2),
                            "description": this.controls[_.findIndex(this.controls, o => o["id"] == c["nist"])]["description"]
                        }
                            : {};
                    })
                    .uniqBy('SlaId')
                    .compact()
                    .value()
                : {};
        }

        getSelectedSecurityControlIds(riskProfile) {
            return _(riskProfile)
                .map(v => v['items'])
                .flatten()
                .map(v => v['risks'])
                .flatten()
                .compact()
                .map(v => v['ccm'])
                .flatten()
                // .map(v => v['id'].split('(')[0])
                .compact()
                .uniq()
                .value();
        }

        saveSelectedDeployment(deployment) {

            var app_id = this.localStorageService.get('session')["applicationId"];
            // Save it to localStorage
            this.localStorageService.set('serviceSelection_' + app_id, deployment);

            if (!!app_id) {
                console.log('deployment', deployment);
                this.Backend.update("serviceselection/" + app_id, deployment)
                    .then(r => {
                        console.log("serviceselection updated", r);
                        var sessionData = this.Session.get();
                        sessionData["dstUrl"] = this.HostConfig.dstHost;
                        sessionData["getDstUrl"] = this.Backend.url() + 'serviceselection/' + sessionData["applicationId"];
                        sessionData["DstTS"] = new Date();
                        this.Session.set(sessionData);
                    });

                // store it into the graph database as well
                this.MACM.runGraphQuery(this.MACM.getCspDeleteQuery(app_id))
                    .then(deleteResponse => {
                        var responses = {};
                        var dataToRun = [];
                        _.each(deployment.data, (componentService, index) => {
                            console.log('c', componentService);
                            const cspData = {
                                "name": componentService["service"]["data"]["Name"],
                                "app_id": app_id
                            };
                            const vmData = {
                                "name": componentService["vmName"],
                                "app_id": app_id
                            };

                            let idx = _.findIndex(dataToRun, csp => csp["name"] == cspData["name"]);
                            if (idx != -1) {
                                dataToRun[idx]["vms"].push(vmData);
                            } else {
                                cspData["vms"] = [];
                                cspData["vms"].push(vmData);
                                dataToRun = [...dataToRun, cspData];
                            }
                        });
                        console.log('dataToRun', dataToRun);
                        return [responses, dataToRun];
                    })
                    .then(([responses, dataToRun]) => {
                        _.each(dataToRun, cspPayload => {
                            this.MACM.runGraphQuery(
                                this.MACM.getCspCreateQuery(cspPayload)
                            ).then(response => responses[cspPayload["name"] + '-create'] = response);
                        });
                        return [responses, dataToRun];
                    })
                    .then(([responses, dataToRun]) => {
                        _.each(dataToRun, cspPayload => {
                            this.MACM.runGraphQuery(
                                this.MACM.getCspVmConnectionQuery(cspPayload)
                            ).then(response => responses[cspPayload["name"] + '-join'] = response);
                        });
                        return responses;
                    })
                    .then(r => console.log('ok', r))
                    .catch(err => console.log('err', err));
            }
        }

        noResults(data) {
            return _.isEmpty(data[0]);
        }

        noLastSelection() {
            return _.isEmpty(this.lastSelection);
        }

    }

    angular.module('musa').controller('ServiceSelectionController', ServiceSelection);
}
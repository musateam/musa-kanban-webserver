/// <reference path="../../typings/index.d.ts" />


module MUSA.Controllers {
    "use strict";

    class Component {
        private columns: Array<Object>;
        private menuOptions: Object;
        private fileUrl;
        private sessionIds: Array<number>;
        private applicationName: string;
        private currentSession: Object;
        private newSessionId: number;

        constructor(public localStorageService
            , private $uibModal: ng.ui.bootstrap.IModalService
            , private $window: ng.IWindowService
            , private Backend: MUSA.IBackend
            , private Auth: MUSA.IAuth
            , private HostConfig: MUSA.IConfig
            , public Session: MUSA.ISessionService
            , public Validation: MUSA.IValidationService
            , public MACM: MUSA.IMacm) {

            // Check if the user is logged in ... if not ... ask for log in
            if (!this.Auth.isLoggedIn()) {
                this.Auth.loginShow();
            }
            // Fetch sessions
            this.sessionIds = [];
            this.currentSession = this.Session.get();

            Backend.fetch("session").then((response) => {
                this.sessionIds = response.data;
                this.newSessionId = parseInt(this.currentSession["applicationId"]);
            });

            this.fileUrl = "";
            this.menuOptions = [
                ['Application Model', function () { this.showDetails({}, "SLA"); }]
                , ['Application SLA', function () { console.log('view 2'); }]
                , ['Application Risk Management', function () { console.log('view 2'); }]
            ];
        }

        generateUUID() {
            var d = new Date().getTime();
            var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = (d + Math.random() * 16) % 16 | 0;
                d = Math.floor(d / 16);
                return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
            });
            return uuid;
        };

        updateSession() {
            this.Session.set(this.currentSession);
        }

        addItem(column) {
            column.items.push(
                {
                    "text": "Component"
                    , "cid": this.generateUUID()
                    , "status": "CREATED"
                    , "getSlaUrl": ""
                    , "getSlaTeamplateUrl": ""
                    , "getAssessedSlaTemplateUrl": ""
                    , "statusOK": {
                        "Model": true,
                        "Risk": true,
                        "Service Selection": true,
                        "Sla": true,
                        "Plan deployment": true,
                        "Deploy": true,
                        "Monitor": true
                    }
                }
            )
            this.Session.set(this.currentSession);
        }

        removeItem(items, index) {
            items.splice(index, 1);
            this.Session.set(this.currentSession);
        }

        dragOptions() {
            return {
                dragEnd: (e) => {
                    this.Session.set(this.currentSession);
                    //this.showDetails(this.currentSession, null, e["dest"]["sortableScope"]["column"]["name"]);
                }
            }
        }

        changeSessionTo(id) {
            this.currentSession["applicationId"] = id;
            this.localStorageService.set("riskProfile", null);
            this.Session.refresh();
            //window.location.reload();
        }

        showDetailsButton(columnName: string) {
            return (columnName == "Execute" || columnName == "SLA" || columnName == "Deploy");
        }

        noComponents(columnName: string) {
            return (columnName != "Model") ?
                !(_.flatten(
                    _.map(
                        _.slice(this.currentSession["columns"],
                            _.findIndex(this.currentSession["columns"], v => v["name"] == columnName))
                        , v => v["items"]))
                    .length > 0)
                : false;
        }

        noComponentsInAll() {
            return _(this.currentSession["columns"]).map(v => v["items"]).flatten().compact().value().length == 0;
        }

        updateComponentsFromMACM() {
            console.log('called Components update');
            var componentsInDashboard = _(this.currentSession["columns"]).map(v => v["items"]).flatten().compact().value();

            this.MACM.runGraphQuery(
                this.MACM.getComponentsQuery(this.currentSession["applicationId"]))
                .then((r) => {
                    _(r["data"])
                        .map(v => v["Values"]["c"]["Properties"])
                        .each(macmComponent => {
                            if (_.findIndex(componentsInDashboard, v => v["cid"] == macmComponent["component_id"]) == -1) {
                                this.addComponent(macmComponent);
                            }
                        });
                });
        }

        addComponent(macmComponent) {
            this.MACM.runGraphQuery(
                this.MACM.getVMForComponentQuery(this.currentSession["applicationId"], macmComponent["component_id"])
            ).then(r => {
                console.log('deploymentMachine', r.data);
                this.currentSession["columns"][0]["items"].push({
                    "text": macmComponent["name"]
                    , "cid": macmComponent["component_id"]
                    , "seccap_provided": macmComponent["seccap_provided"]
                    , "deploymentMachine": (!!r["data"][0]) ? r["data"][0]["Values"]["s"]["Properties"] : {}
                    , "status": "CREATED"
                    , "getSlaUrl": ""
                    , "getSlaTeamplateUrl": ""
                    , "getAssessedSlaTemplateUrl": ""
                    , "statusOK": {
                        "Model": true,
                        "Risk": true,
                        "Service Selection": true,
                        "Sla": true,
                        "Plan deployment": true,
                        "Deploy": true,
                        "Monitor": true
                    }
                });
            });
        }


        showDetails(session, item, columnName: string) {
            var tpl = columnName
                .split(" ")
                .map((w) => w.substring(0, 3).toLowerCase())
                .slice(0, 2)
                .join("")

            this.$uibModal.open({
                templateUrl: 'modal_templates/' + tpl + '.html'
                , size: 'lg'
                , windowClass: 'modal-dialog-full'
                , controller: function ($uibModalInstance, item, session, HostConfig, MACM: MUSA.IMacm, $q: ng.IQService, localStorageService, $timeout: ng.ITimeoutService) {
                    this.item = item;
                    this.session = session;
                    this.HostConfig = HostConfig;
                    this.MACM = MACM;
                    this.localStorageService = localStorageService;
                    this.$timeout = $timeout;

                    this.close = (columnName) => {
                        $uibModalInstance.close();
                        if (columnName == "Model") {
                            this.MACM.updateMACM(this.session["applicationId"]);
                            this.$timeout(() => this.updateComponentsFromMACM(), 5000);
                        }
                    }

                    this.updateComponentsFromMACM = () => {
                        console.log('called Components update udpdate');
                        var componentsInDashboard = _(this.session["columns"]).map(v => v["items"]).flatten().compact().value();

                        this.MACM.runGraphQuery(
                            this.MACM.getComponentsQuery(this.session["applicationId"]))
                            .then((r) => {
                                _(r["data"])
                                    .map(v => v["Values"]["c"]["Properties"])
                                    .each(macmComponent => {
                                        if (_.findIndex(componentsInDashboard, v => v["cid"] == macmComponent["component_id"]) == -1) {
                                            this.addComponent(macmComponent);
                                        }
                                    });
                            });
                    }

                    this.addComponent = (macmComponent) => {
                        this.MACM.runGraphQuery(
                            this.MACM.getVMForComponentQuery(this.session["applicationId"], macmComponent["component_id"])
                        ).then(r => {
                            console.log('deploymentMachine', r.data);
                            this.session["columns"][0]["items"].push({
                                "text": macmComponent["name"]
                                , "cid": macmComponent["component_id"]
                                , "seccap_provided": macmComponent["seccap_provided"]
                                , "deploymentMachine": (!!r["data"][0]) ? r["data"][0]["Values"]["s"]["Properties"] : {}
                                , "status": "CREATED"
                                , "getSlaUrl": ""
                                , "getSlaTeamplateUrl": ""
                                , "getAssessedSlaTemplateUrl": ""
                                , "statusOK": {
                                    "Model": true,
                                    "Risk": true,
                                    "Service Selection": true,
                                    "Sla": true,
                                    "Plan deployment": true,
                                    "Deploy": true,
                                    "Monitor": true
                                }
                            });
                        });
                    }

                    this.pushToArray = function (arr, object) {
                        arr.push(object);
                    }

                    this.redirect = function (url) {
                        window.open(url, '_blank');
                    }

                    this.redirectlocal = function (url) {
                        window.open(window.location.origin + '/' + url, '_blank');
                    }
                }
                , controllerAs: 'm'
                , resolve: {
                    item: () => item,
                    session: () => session
                }
            }).closed.then(() => {
                this.Session.refresh();
                this.Validation.updateAllValidations();
            });
        }

        clearSession() {
            this.Session.set({});
            this.localStorageService.set('riskProfile', "");
            window.location.reload();
        }
    }

    angular.module('musa').controller('ComponentController', Component);
}

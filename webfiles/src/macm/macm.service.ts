/// <reference path="../../typings/index.d.ts" />

module MUSA.Controllers {
    "use strict";

    class MACM implements IMacm {

        private GraphUrl: string;

        constructor(public localStorageService
            , private Backend: MUSA.IBackend
            , private HostConfig: MUSA.IConfig
            , private $q: ng.IQService
            , private $timeout: ng.ITimeoutService
        ) {
            this.GraphUrl = 'graph/query';
        }

        fetchMACMCreate(applicationID) {
            return this.Backend.fetchExt(this.HostConfig.modellerHost + "eu.musa.modeller.ws/webresources/camelfilews/getCamel/" + applicationID + "/MACM");
        }

        getMACMDeleteQuery(applicationID) {
            return `MATCH (n)
                    WHERE n.app_id = "${applicationID}" 
                    DETACH DELETE n`;
        }

        getIaaSMachinesQuery(applicationID) {
            return `MATCH (n:IaaS)
                    WHERE n.app_id = ${applicationID}
                    RETURN {
                        name: n.name,
                        location: n.location
                    }`;
        }

        getComponentSecReqQuery(applicationID, componentID) {
            return `MATCH (n:service)
                    WHERE n.app_id = "${applicationID}"
                    AND n.component_id = "${componentID}"
                    RETURN n.seccap_provided`;
        }

        getVMForComponentQuery(applicationID, componentID) {
            return `MATCH (s:IaaS)-[:hosts*1..2]->(n:service)
                    WHERE n.app_id = "${applicationID}"
                    AND n.component_id = "${componentID}"
                    RETURN s`;
        }

        getComponentsQuery(applicationID) {
            return `MATCH (c)
                    WHERE c.app_id = "${applicationID}"
                    AND EXISTS(c.component_id)
                    AND NOT c.type =~ "AGENT.*"
                    RETURN c`;
        }

        prepareCspPayload(dataToRun) {
            let payload = {
                "name": dataToRun["name"],
                "app_id": dataToRun["app_id"].toString()
            };
            var payloadString = JSON.stringify(payload);
            payloadString = payloadString.replace(/\"([^(\")"]+)\":/g, "$1:");
            return payloadString;
        }

        getCspDeleteQuery(app_id) {
            return `MATCH (csp:CSP)
                    WHERE csp.app_id = "${app_id}"
                    DETACH DELETE csp`;
        }

        getCspCreateQuery(dataToRun) {
            return `CREATE (csp:CSP ${this.prepareCspPayload(dataToRun)})`;
        }

        getCspVmConnectionQuery(dataToRun) {
            var queryStringStart = `MATCH (csp:CSP ${this.prepareCspPayload(dataToRun)}) `;
            var queryStringWhere = "";
            var queryStringEnd = "";
            _.each(dataToRun["vms"], (vm, k) => {
                vm["app_id"] = vm["app_id"].toString();
                let joiner = (queryStringWhere == "") ? " WHERE " : " AND ";
                queryStringStart += ` , (vm${k})`;
                queryStringWhere += joiner + `vm${k}.name = "${vm["name"]}"`;
                queryStringWhere += ` AND vm${k}.app_id = "${vm["app_id"]}"`;
                queryStringEnd += ` CREATE (csp)-[:provides]->(vm${k})`;
            });
            // Remove the quotes on the arguments 

            // return `
            // CREATE (csp:CSP ${cspString})
            // WITH (csp)
            // MATCH (vm:IaaS:service {name:’<vmname>’, app_id:’<appid>’}) CREATE (csp) -[:provides]->(vm)
            //         `;

            // return `MERGE (csp:CSP ${cspString})
            //         MERGE (vm ${vmString})
            //         MERGE (csp)-[:provides]->(vm)`;
            return queryStringStart + queryStringWhere + queryStringEnd;
        }

        runGraphQuery(query) {
            return this.Backend.update(this.GraphUrl, query);
        }

        updateMACM(applicationID) {
            this.fetchMACMCreate(applicationID)
                .then(fetchResult => {
                    var deleteResult = this.runGraphQuery(this.getMACMDeleteQuery(applicationID)).then(r => r);
                    return {
                        "fetchResult": fetchResult,
                        "deleteResult": deleteResult
                    };
                }).then(results => {
                    this.runGraphQuery(results["fetchResult"]["data"]).then(r => results["runNewMacmResult"] = r);
                    return results;
                })
                .then(ok => {
                    console.log("ok", ok);
                })
                .catch(err => {
                    console.log('err', err)
                });
        }
    }
    angular.module('musa').service('MACM', (localStorageService, Backend, HostConfig, $q, $timeout) => new MACM(localStorageService, Backend, HostConfig, $q, $timeout));
}
/// <reference path="../typings/index.d.ts" />

declare module 'MUSA' {
    export = MUSA;
}

declare module MUSA {

    interface IBackend {
        url(): string;
        fetch(endPoint: string): ng.IHttpPromise<any>;
        insert(endPoint: string, data: any): ng.IHttpPromise<any>;
        update(endPoint: string, data: any): ng.IHttpPromise<any>;
        remove(endPoint: string): ng.IHttpPromise<any>;
        fetchExt(endPoint: string): ng.IHttpPromise<any>;
        insertExt(endPoint: string, data: any): ng.IHttpPromise<any>;
        updateExt(endPoint: string, data: any): ng.IHttpPromise<any>;
        removeExt(endPoint: string): ng.IHttpPromise<any>;
    }

    interface IUrl {
        host: string;
        port: number;
        protocol: string;
        path(): string;
    }

    interface IColumn {
        name: string;
        items: Array<Object>;
    }

    interface ISession {
        applicationId: number;
        applicationName: string;
        columns: Array<IColumn>
    }

    interface IConfig {
        modellerHost: IUrl;
        riskHost: IUrl;
        dstHost: IUrl;
        deployerHost: IUrl;
        slaGeneratorHost: IUrl;
        monitorHost: IUrl;
        dashboardHost: IUrl;
    }

    interface IAuth {
        loginShow(): any;
        logout(): void;
        isLoggedIn(): boolean;
    }

    interface IMacm {
        fetchMACMCreate(applicationId: string): ng.IPromise<any>;
        getMACMDeleteQuery(applicationId: string): string;
        getIaaSMachinesQuery(applicationId: string): string;
        getComponentSecReqQuery(applicationId: string, componentName: string): string;
        getVMForComponentQuery(applicationId: string, componentName: string): string;
        getComponentsQuery(applicationId: string): string;
        prepareCspPayload(dataToRun: Object): string;
        getCspDeleteQuery(applicationId: string): string;
        getCspCreateQuery(dataToRun: Object): string;
        getCspVmConnectionQuery(dataToRun: Object): string;
        runGraphQuery(query: string): ng.IPromise<any>;
        updateMACM(applicationId: string): void;
    }

    interface ISessionService {
        get(): object;
        set(sessionIData: object): object;
        refresh(): object;
    }

    interface IValidationService {
        updateItemsValidation(key: string, value: boolean, itemToUpdate: string): void;
        validateModel(): void;
        validateServiceSellection(): void;
        validationMessage(columnName: string): string;
        updateAllValidations(): void;
    }
}
/// <reference path="../../typings/index.d.ts" />


module MUSA.Controllers {
    "use strict";

    class Risk {
        private riskProfile: Array<Object>;
        private currentSession: Object;
        private routeSelectedComponent: string;

        constructor(public localStorageService
            , private $uibModal: ng.ui.bootstrap.IModalService
            , private $window: ng.IWindowService
            , private Backend: MUSA.IBackend
            , private HostConfig: MUSA.IConfig
            , private $location: ng.ILocationService
            , private Session: MUSA.ISessionService
            , private Validation: MUSA.IValidationService) {
            this.riskProfile = this.localStorageService.get('riskProfile') || [];
            this.currentSession = this.localStorageService.get('session');

            console.log(this.currentSession);
            this.routeSelectedComponent = this.$location.search()["component"];

            if (_.isEmpty(this.riskProfile)) {
                this.Backend.fetch("riskprofile/" + this.currentSession["applicationId"]).then((response) => {
                    this.riskProfile = response.data;
                    this.updateComponents();
                    this.updateRisks();
                }, (error) => {
                    this.riskProfile = [
                        { "name": "Components Description", "items": [] }
                        , { "name": "STRIDE based Risk Assessment", "items": [] }
                        , { "name": "Security Controls", "items": [] }
                        , { "name": "Validation", "items": [] }];

                    this.updateComponents();
                    this.localStorageService.set('riskProfile', this.riskProfile);
                    this.Backend.insert("riskprofile/" + this.currentSession["applicationId"], this.riskProfile);
                });
            } else {
                this.Backend.fetch("riskprofile/" + this.currentSession["applicationId"]).then((response) => {
                    this.riskProfile = response.data;
                    this.updateComponents();
                    this.updateRisks();
                });
            }
        }

        orderByStride(i) { return ["SPOOFING", "TAMPERING", "REPUDIATION", "INFORMATION DISCLOSURE", "DENIAL OF SERVICE", "ELEVATION OF PRIVILEGES"].indexOf(i["name"]) };

        updateComponents() {
            var currentComponents = _.flatten(_.map(_.slice(this.currentSession["columns"], _.findIndex(this.currentSession["columns"], v => v["name"] == "Risk")), v => v["items"]));
            var currentRiskComponents = _.flatten(_.map(this.riskProfile, v => v["items"]));

            // Add components from the current session
            _.each(currentComponents, (component) => {
                if (_.findIndex(currentRiskComponents, v => v["cid"] == component["cid"]) == -1) {
                    this.riskProfile[0]["items"].push(component);
                }
            });

            // Remove components if they do not exist anymore
            // go over each riskProfile column
            _.each(this.riskProfile, (riskColumn, riskColumnIndex) => {
                riskColumn["items"].forEach((item, itemsIndex) => {
                    if (_.findIndex(currentComponents, v => v["cid"] == item["cid"]) == -1) {
                        this.riskProfile[riskColumnIndex]["items"].splice(itemsIndex, 1);
                    }
                });
            });
        }

        clearSession() {
            this.riskProfile = [];
            this.localStorageService.clearAll();
            window.location.reload();
        }

        updateRisks() {
            this.localStorageService.set('riskProfile', this.riskProfile);
            this.Backend.update("riskprofile/" + this.currentSession["applicationId"], this.riskProfile).then(r => console.log("riskProfile updated", r));
            this.currentSession["getRiskUrl"] = this.Backend.url() + "riskprofile/" + this.currentSession["applicationId"];
            this.currentSession["RiskTS"] = new Date();
            this.Session.set(this.currentSession);
        }

        updateSession() {
            this.Session.set(this.currentSession);
        }

        updateComponent(componentData) {
            let data = componentData;
            delete data["OWASPModel"];
            this.Backend.update("riskprofile/" + this.currentSession["applicationId"] + "/component/" + componentData["cid"], data);
        }

        countThreats(item) {

            var getPositionOfAnItemInASession = (item) => {
                var pos = [];

                _.each(this.currentSession["columns"], (column, index) => {
                    var location = _.findIndex(column["items"], sessionItem => sessionItem["cid"] == item["cid"]);
                    if (location != -1) { pos.push(index); pos.push(location); }
                });
                return pos;
            }

            var changedItemToProfiled = (item) => {
                var pos = getPositionOfAnItemInASession(item);
                this.currentSession["columns"][pos[0]]["items"][pos[1]]["status"] = "PROFILED";
                //this.Session.set(this.currentSession);
            }


            if (!!item.risks) {
                var totalRisks = item.risks.length;
                var risksResolved = _(item.risks)
                    .filter(r => r["roam"] == "Resolved")
                    .value()
                    .length;
                var risksAddressed = _(item.risks)
                    .filter(r => (r["roam"] == "Accepted" || r["roam"] == "Mitigated"))
                    .value()
                    .length;
                var count = (totalRisks - risksResolved == risksAddressed) ? -1 : (totalRisks - risksResolved - risksAddressed);
                if (count == -1) {
                    changedItemToProfiled(item);
                    item["status"] = "PROFILED";

                    // Set the validationStatus to OK
                    this.Validation.updateItemsValidation("Risk", true, item);
                } else {
                    this.Validation.updateItemsValidation("Risk", false, item);
                }
                return count;
            } else {
                return 0;
            }
        }

        countThreatsMessage(item) {
            var count = this.countThreats(item);
            return (count == 0) ? "Analysis Needed!" : (count == 1) ? "Analysis required for " + count + " threat" : "Analysis required for " + count + " threats";
        }

        itemRiskSeverity(item, level) {
            var totalLC = 0;
            if (!!item.risks) {
                item.risks.forEach(LC => totalLC += ((LC["likelihood"] || 0) + (LC["impact"] || 0)) * 0.125);
                var sumLC = totalLC / item.risks.length;
                var result = false;
                switch (level) {
                    case 'low':
                        result = (sumLC < 6);
                        break;
                    case 'medium':
                        result = (sumLC < 12 && sumLC >= 6);
                        break;
                    case 'high':
                        result = (sumLC >= 12);
                        break;
                }
            } else {
                result = level == 'no';
            }
            return result;
        }

        itemMitigationLevel(item, level) {
            var totalSR = 0;
            if (!!item.risks) {
                item.risks.forEach(s => totalSR += !!s["sR"] ? s["sR"].length : 0);

                var itemSR = totalSR / item.risks.length;
                var result = false;
                switch (level) {
                    case 'low':
                        result = (itemSR >= 2);
                        break;
                    case 'medium':
                        result = (itemSR > 0 && itemSR < 2);
                        break;
                    case 'high':
                        result = (itemSR == 0);
                        break;
                }
                return result;
            }
            else {
                return level == 'no';
            }
        }

        dragOptions() {
            return {
                dragEnd: (e) => {
                    this.updateComponent(e['source']['itemScope']['item']);
                    this.updateRisks();
                }
            }
        }

        showDetails(item, columnName: string) {
            var tpl = columnName
                .split(" ")
                .map((w) => w.substring(0, 3).toLowerCase())
                .slice(0, 2)
                .join("")

            this.$uibModal.open({
                templateUrl: 'modal_templates/' + tpl + '.html'
                , size: 'lg'
                , controller: function ($uibModalInstance, item, localStorageService, Backend) {
                    this.item = item;
                    this.localStorageService = localStorageService;
                    this.Backend = Backend;
                    this.threats = [];
                    this.strideCategories = [];
                    this.securityControls = [];
                    this.suggestedControls = [];

                    this.fetchData = (url, placeholder) => this.Backend.fetch(url).then((r) => this[placeholder] = r.data);

                    this.certificates =
                        [
                            { "id": "ISO", "name": "ISO" }
                            , { "id": "Nist", "name": "Nist" }
                            , { "id": "Cobit", "name": "Cobit" }
                            , { "id": "Hippa", "name": "Hippa" }
                        ];

                    this.orderByStride = (i) => ["SPOOFING", "TAMPERING", "REPUDIATION", "INFORMATION DISCLOSURE", "DENIAL OF SERVICE", "ELEVATION OF PRIVILEGES"].indexOf(i["name"]);


                    this.OWASPModel =
                        [
                            {
                                "children": [
                                    {
                                        "children": [
                                            {
                                                "description": " How technically skilled is this group of threat agents? Security penetration skills (9), network and programming skills (6), advanced computer user (5), some technical skills (3), no technical skills (1)",
                                                "name": "Skill level",
                                                "id": "skillLevel"
                                            },
                                            {
                                                "description": " How motivated is this group of threat agents to find and exploit this vulnerability? Low or no reward (1), possible reward (4), high reward (9)",
                                                "name": "Motive",
                                                "id": "motive"
                                            },
                                            {
                                                "description": " What resources and opportunities are required for this group of threat agents to find and exploit this vulnerability? Full access or expensive resources required (0), special access or resources required (4), some access or resources required (7), no access or resources required (9)",
                                                "name": "Opportunity",
                                                "id": "opportunity"
                                            },
                                            {
                                                "description": " How large is this group of threat agents? Developers (2), system administrators (2), intranet users (4), partners (5), authenticated users (6), anonymous Internet users (9)",
                                                "name": "Size",
                                                "id": "size"
                                            }
                                        ],
                                        "description": "The first set of factors are related to the threat agent involved. The goal here is to estimate the likelihood of a successful attack by this group of threat agents. Use the worst-case threat agent.",
                                        "name": "Threat Agent Factory"
                                    },
                                    {
                                        "children": [
                                            {
                                                "description": " How easy is it for this group of threat agents to discover this vulnerability? Practically impossible (1), difficult (3), easy (7), automated tools available (9)",
                                                "name": "Ease of discovery"
                                                , "id": "easeOfDiscovery"
                                            },
                                            {
                                                "description": " How easy is it for this group of threat agents to actually exploit this vulnerability? Theoretical (1), difficult (3), easy (5), automated tools available (9)",
                                                "name": "Ease of exploit"
                                                , "id": "easeOfExploit"
                                            },
                                            {
                                                "description": " How well known is this vulnerability to this group of threat agents? Unknown (1), hidden (4), obvious (6), public knowledge (9)",
                                                "name": "Awareness"
                                                , "id": "awareness"
                                            },
                                            {
                                                "description": " How likely is an exploit to be detected? Active detection in application (1), logged and reviewed (3), logged without review (8), not logged (9)",
                                                "name": "Intrusion detection"
                                                , "id": "intrusionDetection"
                                            }
                                        ],
                                        "description": "The next set of factors are related to the vulnerability involved. The goal here is to estimate the likelihood of the particular vulnerability involved being discovered and exploited. Assume the threat agent selected above.",
                                        "name": "Vulnerability Factors"
                                    }
                                ],
                                "description": "At the highest level, this is a rough measure of how likely this particular vulnerability is to be uncovered and exploited by an attacker.",
                                "name": "likelihood"
                            },
                            {
                                "children": [
                                    {
                                        "children": [
                                            {
                                                "description": " How much data could be disclosed and how sensitive is it? Minimal non-sensitive data disclosed (2), minimal critical data disclosed (6), extensive non-sensitive data disclosed (6), extensive critical data disclosed (7), all data disclosed (9)",
                                                "name": "Loss of confidentiality"
                                                , "id": "lossOfConfidentiality"
                                            },
                                            {
                                                "description": " How much data could be corrupted and how damaged is it? Minimal slightly corrupt data (1), minimal seriously corrupt data (3), extensive slightly corrupt data (5), extensive seriously corrupt data (7), all data totally corrupt (9)",
                                                "name": "Loss of integrity"
                                                , "id": "lossOfIntegrity"
                                            },
                                            {
                                                "description": " How much service could be lost and how vital is it? Minimal secondary services interrupted (1), minimal primary services interrupted (5), extensive secondary services interrupted (5), extensive primary services interrupted (7), all services completely lost (9)",
                                                "name": "Loss of availability"
                                                , "id": "lossOfAvailability"
                                            },
                                            {
                                                "description": " Are the threat agents' actions traceable to an individual? Fully traceable (1), possibly traceable (7), completely anonymous (9)",
                                                "name": "Loss of accountability"
                                                , "id": "lossOfAccountability"
                                            }
                                        ],
                                        "description": "Technical impact can be broken down into factors aligned with the traditional security areas of concern: confidentiality, integrity, availability, and accountability. The goal is to estimate the magnitude of the impact on the system if the vulnerability were to be exploited.",
                                        "name": "Technical Impact Factors"
                                    },
                                    {
                                        "children": [
                                            {
                                                "description": " How much financial damage will result from an exploit? Less than the cost to fix the vulnerability (1), minor effect on annual profit (3), significant effect on annual profit (7), bankruptcy (9)",
                                                "name": "Financial damage"
                                                , "id": "financialDamage"
                                            },
                                            {
                                                "description": " Would an exploit result in reputation damage that would harm the business? Minimal damage (1), Loss of major accounts (4), loss of goodwill (5), brand damage (9)",
                                                "name": "Reputation damage"
                                                , "id": "reputationDamage"
                                            },
                                            {
                                                "description": " How much exposure does non-compliance introduce? Minor violation (2), clear violation (5), high profile violation (7)",
                                                "name": "Non-compliance"
                                                , "id": "nonCompliance"
                                            },
                                            {
                                                "description": " How much personally identifiable information could be disclosed? One individual (3), hundreds of people (5), thousands of people (7), millions of people (9)",
                                                "name": "Privacy violation"
                                                , "id": "privacyViolation"
                                            }
                                        ],
                                        "description": " The business impact stems from the technical impact, but requires a deep understanding of what is important to the company running the application. In general, you should be aiming to support your risks with business impact, particularly if your audience is executive level. The business risk is what justifies investment in fixing security problems.",
                                        "name": "Business Impact Factors"
                                    }
                                ],
                                "description": "When considering the impact of a successful attack, it's important to realize that there are two kinds of impacts. The first is the \"technical impact\" on the application, the data it uses, and the functions it provides. The other is the \"business impact\" on the business and company operating the application.",
                                "name": "impact"
                            }
                        ];

                    this.item.sRA = !!this.item.sRA ? this.item.sRA : [];
                    this.item.OWASPModel = !!this.item.OWASPModel ? this.item.OWASPModel : this.OWASPModel;

                    this.close = function () {
                        $uibModalInstance.close();
                    }

                    this.showOnlySuggested = true;
                    this.showDescriptions = true;

                    this.pushToArray = function (arr, object, key = null, keyValue = '') {
                        // if (arr.indexOf(object) == -1) {
                        if (_.isNull(key)) {
                            if (_.indexOf(arr, object) == -1) {
                                arr.push(object);
                            }
                        } else {
                            if (_.isEmpty(_.find(arr, (i) => i[key] == keyValue))) {
                                arr.push(object);
                            }
                        }
                    }

                    this.logStat = () => {
                        var data = [];
                        _.each(arguments, (i) => {
                            data.push(i);
                            data.push("<===");
                        });
                        this.Backend.update("riskprofile/log", JSON.stringify(data, null, '\t'));
                    }

                    this.fetchCcmControls = (component, nistControl) => {
                        this.Backend.fetch(`nist/${nistControl['id']}/ccm`).then(r => {
                            if (!!component.ccm) {
                                _.each(r.data, v => this.pushToArray(component.ccm, v));
                            } else {
                                component.ccm = r.data;
                            }

                            // TEMP: double store for the mapping back
                            if (!component.ccmMap) {
                                component.ccmMap = [];
                            }
                            component.ccmMap.push({
                                "nist": nistControl["id"]
                                , "ccm": r.data
                            });


                        });
                        console.log('component', component);
                    }

                    this.onThreatChange = () => {
                        this.suggestedControls = [];
                        this.Backend.fetch('slaeditor/threat/' + this.addToRisk['id'] + '/controls').then((r) => {
                            this.suggestedControls = r.data.map(v => v['id']);
                        });
                    }

                    this.securityControlFilter = (item) => {
                        if (this.showOnlySuggested && !!this.addToRisk) {
                            return (this.suggestedControls.indexOf(item['id']) > -1);
                        } else {
                            return true;
                        }
                    }

                    this.addRisk = () => {
                        var risks = item.risks || [];

                        risks.push({
                            "name": ""
                            , "probability": 1
                            , "impact": 1
                        });

                        item.risks = risks;
                    }

                    this.calculateRiskSeverity = function (riskName, level) {
                        // filter our the risk
                        var risk = this.item.risks.filter((v) => v.name == riskName);

                        // calculate
                        var LC = _.filter(risk, (v) => v["name"] == riskName)[0];
                        // return true or false depending on the level
                        var sumLC = ((LC["likelihood"] || 0) + (LC["impact"] || 0)) * 0.125;

                        var result = false;
                        switch (level) {
                            case 'low':
                                result = (sumLC < 6);
                                break;
                            case 'medium':
                                result = (sumLC < 12 && sumLC >= 6);
                                break;
                            case 'high':
                                result = (sumLC >= 12);
                                break;
                        }
                        return result;
                    }

                    this.removeItem = (array, risk) => {
                        if (_.indexOf(array, risk) != -1) {
                            array.splice(_.indexOf(array, risk), 1);
                        }
                    }

                    this.compileCSAControls = function () {

                        // cleanup the item to extract the parents
                        // remove duplicates
                        var SRI = _(this.item["risks"])
                            .map(o => o["sR"])
                            .flatten()
                            .extend(this.item["sRA"])
                            .uniqBy(e => e.id)
                            .value();

                        var sR = _.uniqBy(_.extend(_.flatten(_.map(this.item["risks"], o => o["sR"])), this.item["sRA"]), e => e['id']);

                        console.log("SRI", SRI, "sR", sR);

                        sR = _.each(sR, (v) => {
                            v['children'] = [];
                            var description = v['name'];
                            v['name'] = v['id'];
                            v['description'] = description;
                            v['children'].push(this.CSAMetrics[Math.floor(Math.random() * this.CSAMetrics.length)]);
                        });

                        var data = {
                            name: 'ROOT'
                            , id: 'root'
                            , children: sR
                        };

                        return data;
                    }


                    this.sumOWASP = function () {
                        // for each of the 1st level OWASP dimensions
                        _.each(this.item.OWASPModel, (LI) => {
                            // for each of the risk
                            _.each(this.item.risks, (risk) => {

                                // extract children
                                var group = this.item.OWASPModel.filter(v => v.name == LI.name);
                                var groupValues = _.flatMap(group[0].children, v => v.children.map((v1) => v1.id));

                                // cleanup values
                                _.remove(groupValues, _.isUndefined);

                                // filter user values from OWASP model
                                var userSetValues = _.pickBy(risk["owasp"], function (value, key) {
                                    return _.includes(groupValues, key);
                                });

                                // calculate probability and impact value
                                var indexOfRisk = _.findIndex(this.item.risks, (v) => v["name"] == risk.name);
                                this.item.risks[indexOfRisk][LI.name] = _(userSetValues).values().sum();
                            });
                        });
                    }

                    this.slider = {
                        options: {
                            floor: 0,
                            ceil: 9,
                            step: 1,
                            showTicks: true,
                            showSelectionBar: true,
                            getSelectionBarColor: function (value) {
                                if (value <= 3)
                                    return 'green';
                                if (value <= 6)
                                    return 'orange';
                                if (value <= 9)
                                    return 'red';
                                return '#2AE02A';
                            },
                            getPointerColor: function (value) {
                                if (value <= 3)
                                    return 'green';
                                if (value <= 6)
                                    return 'orange';
                                if (value <= 9)
                                    return 'red';
                                return '#2AE02A';
                            },
                            stepsArray: [
                                { value: 0, legend: 'Very Unlikely' },
                                { value: 1 },
                                { value: 2 },
                                { value: 3 },
                                { value: 4 },
                                { value: 5 },
                                { value: 6 },
                                { value: 7 },
                                { value: 8 },
                                { value: 9, legend: 'Very Likely' }
                            ],
                            onChange: () => this.sumOWASP()
                        }
                    }
                }
                , controllerAs: 'm'
                , resolve: {
                    item: () => item
                }
            }).closed.then(() => {
                this.updateRisks();
                this.updateSession();
            })
        }
    }
    angular.module('musa').controller('RiskController', Risk);
}

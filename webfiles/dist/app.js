/// <reference path="../typings/index.d.ts" />
var MUSA;
(function (MUSA) {
    "use strict";
    angular.module('musa', [
        'ng',
        'ui.router',
        'xeditable',
        'LocalStorageModule',
        'as.sortable',
        'ui.bootstrap',
        'd3-multi-parent',
        'ui.bootstrap.contextMenu',
        'ui.select',
        'ngSanitize',
        'toggle-switch',
        'rzModule',
        'auth0.lock',
        'angular-jwt'
    ]);
})(MUSA || (MUSA = {}));
/// <reference path="../../typings/index.d.ts" />
var MUSA;
(function (MUSA) {
    var Controllers;
    (function (Controllers) {
        "use strict";
        var Component = /** @class */ (function () {
            function Component(localStorageService, $uibModal, $window, Backend, Auth, HostConfig, Session, Validation, MACM) {
                var _this = this;
                this.localStorageService = localStorageService;
                this.$uibModal = $uibModal;
                this.$window = $window;
                this.Backend = Backend;
                this.Auth = Auth;
                this.HostConfig = HostConfig;
                this.Session = Session;
                this.Validation = Validation;
                this.MACM = MACM;
                // Check if the user is logged in ... if not ... ask for log in
                if (!this.Auth.isLoggedIn()) {
                    this.Auth.loginShow();
                }
                // Fetch sessions
                this.sessionIds = [];
                this.currentSession = this.Session.get();
                Backend.fetch("session").then(function (response) {
                    _this.sessionIds = response.data;
                    _this.newSessionId = parseInt(_this.currentSession["applicationId"]);
                });
                this.fileUrl = "";
                this.menuOptions = [
                    ['Application Model', function () { this.showDetails({}, "SLA"); }],
                    ['Application SLA', function () { console.log('view 2'); }],
                    ['Application Risk Management', function () { console.log('view 2'); }]
                ];
            }
            Component.prototype.generateUUID = function () {
                var d = new Date().getTime();
                var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                    var r = (d + Math.random() * 16) % 16 | 0;
                    d = Math.floor(d / 16);
                    return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
                });
                return uuid;
            };
            ;
            Component.prototype.updateSession = function () {
                this.Session.set(this.currentSession);
            };
            Component.prototype.addItem = function (column) {
                column.items.push({
                    "text": "Component",
                    "cid": this.generateUUID(),
                    "status": "CREATED",
                    "getSlaUrl": "",
                    "getSlaTeamplateUrl": "",
                    "getAssessedSlaTemplateUrl": "",
                    "statusOK": {
                        "Model": true,
                        "Risk": true,
                        "Service Selection": true,
                        "Sla": true,
                        "Plan deployment": true,
                        "Deploy": true,
                        "Monitor": true
                    }
                });
                this.Session.set(this.currentSession);
            };
            Component.prototype.removeItem = function (items, index) {
                items.splice(index, 1);
                this.Session.set(this.currentSession);
            };
            Component.prototype.dragOptions = function () {
                var _this = this;
                return {
                    dragEnd: function (e) {
                        _this.Session.set(_this.currentSession);
                        //this.showDetails(this.currentSession, null, e["dest"]["sortableScope"]["column"]["name"]);
                    }
                };
            };
            Component.prototype.changeSessionTo = function (id) {
                this.currentSession["applicationId"] = id;
                this.localStorageService.set("riskProfile", null);
                this.Session.refresh();
                //window.location.reload();
            };
            Component.prototype.showDetailsButton = function (columnName) {
                return (columnName == "Execute" || columnName == "SLA" || columnName == "Deploy");
            };
            Component.prototype.noComponents = function (columnName) {
                return (columnName != "Model") ?
                    !(_.flatten(_.map(_.slice(this.currentSession["columns"], _.findIndex(this.currentSession["columns"], function (v) { return v["name"] == columnName; })), function (v) { return v["items"]; }))
                        .length > 0)
                    : false;
            };
            Component.prototype.noComponentsInAll = function () {
                return _(this.currentSession["columns"]).map(function (v) { return v["items"]; }).flatten().compact().value().length == 0;
            };
            Component.prototype.updateComponentsFromMACM = function () {
                var _this = this;
                console.log('called Components update');
                var componentsInDashboard = _(this.currentSession["columns"]).map(function (v) { return v["items"]; }).flatten().compact().value();
                this.MACM.runGraphQuery(this.MACM.getComponentsQuery(this.currentSession["applicationId"]))
                    .then(function (r) {
                    _(r["data"])
                        .map(function (v) { return v["Values"]["c"]["Properties"]; })
                        .each(function (macmComponent) {
                        if (_.findIndex(componentsInDashboard, function (v) { return v["cid"] == macmComponent["component_id"]; }) == -1) {
                            _this.addComponent(macmComponent);
                        }
                    });
                });
            };
            Component.prototype.addComponent = function (macmComponent) {
                var _this = this;
                this.MACM.runGraphQuery(this.MACM.getVMForComponentQuery(this.currentSession["applicationId"], macmComponent["component_id"])).then(function (r) {
                    console.log('deploymentMachine', r.data);
                    _this.currentSession["columns"][0]["items"].push({
                        "text": macmComponent["name"],
                        "cid": macmComponent["component_id"],
                        "seccap_provided": macmComponent["seccap_provided"],
                        "deploymentMachine": (!!r["data"][0]) ? r["data"][0]["Values"]["s"]["Properties"] : {},
                        "status": "CREATED",
                        "getSlaUrl": "",
                        "getSlaTeamplateUrl": "",
                        "getAssessedSlaTemplateUrl": "",
                        "statusOK": {
                            "Model": true,
                            "Risk": true,
                            "Service Selection": true,
                            "Sla": true,
                            "Plan deployment": true,
                            "Deploy": true,
                            "Monitor": true
                        }
                    });
                });
            };
            Component.prototype.showDetails = function (session, item, columnName) {
                var _this = this;
                var tpl = columnName
                    .split(" ")
                    .map(function (w) { return w.substring(0, 3).toLowerCase(); })
                    .slice(0, 2)
                    .join("");
                this.$uibModal.open({
                    templateUrl: 'modal_templates/' + tpl + '.html',
                    size: 'lg',
                    windowClass: 'modal-dialog-full',
                    controller: function ($uibModalInstance, item, session, HostConfig, MACM, $q, localStorageService, $timeout) {
                        var _this = this;
                        this.item = item;
                        this.session = session;
                        this.HostConfig = HostConfig;
                        this.MACM = MACM;
                        this.localStorageService = localStorageService;
                        this.$timeout = $timeout;
                        this.close = function (columnName) {
                            $uibModalInstance.close();
                            if (columnName == "Model") {
                                _this.MACM.updateMACM(_this.session["applicationId"]);
                                _this.$timeout(function () { return _this.updateComponentsFromMACM(); }, 5000);
                            }
                        };
                        this.updateComponentsFromMACM = function () {
                            console.log('called Components update udpdate');
                            var componentsInDashboard = _(_this.session["columns"]).map(function (v) { return v["items"]; }).flatten().compact().value();
                            _this.MACM.runGraphQuery(_this.MACM.getComponentsQuery(_this.session["applicationId"]))
                                .then(function (r) {
                                _(r["data"])
                                    .map(function (v) { return v["Values"]["c"]["Properties"]; })
                                    .each(function (macmComponent) {
                                    if (_.findIndex(componentsInDashboard, function (v) { return v["cid"] == macmComponent["component_id"]; }) == -1) {
                                        _this.addComponent(macmComponent);
                                    }
                                });
                            });
                        };
                        this.addComponent = function (macmComponent) {
                            _this.MACM.runGraphQuery(_this.MACM.getVMForComponentQuery(_this.session["applicationId"], macmComponent["component_id"])).then(function (r) {
                                console.log('deploymentMachine', r.data);
                                _this.session["columns"][0]["items"].push({
                                    "text": macmComponent["name"],
                                    "cid": macmComponent["component_id"],
                                    "seccap_provided": macmComponent["seccap_provided"],
                                    "deploymentMachine": (!!r["data"][0]) ? r["data"][0]["Values"]["s"]["Properties"] : {},
                                    "status": "CREATED",
                                    "getSlaUrl": "",
                                    "getSlaTeamplateUrl": "",
                                    "getAssessedSlaTemplateUrl": "",
                                    "statusOK": {
                                        "Model": true,
                                        "Risk": true,
                                        "Service Selection": true,
                                        "Sla": true,
                                        "Plan deployment": true,
                                        "Deploy": true,
                                        "Monitor": true
                                    }
                                });
                            });
                        };
                        this.pushToArray = function (arr, object) {
                            arr.push(object);
                        };
                        this.redirect = function (url) {
                            window.open(url, '_blank');
                        };
                        this.redirectlocal = function (url) {
                            window.open(window.location.origin + '/' + url, '_blank');
                        };
                    },
                    controllerAs: 'm',
                    resolve: {
                        item: function () { return item; },
                        session: function () { return session; }
                    }
                }).closed.then(function () {
                    _this.Session.refresh();
                    _this.Validation.updateAllValidations();
                });
            };
            Component.prototype.clearSession = function () {
                this.Session.set({});
                this.localStorageService.set('riskProfile', "");
                window.location.reload();
            };
            return Component;
        }());
        angular.module('musa').controller('ComponentController', Component);
    })(Controllers = MUSA.Controllers || (MUSA.Controllers = {}));
})(MUSA || (MUSA = {}));
/// <reference path="../../typings/index.d.ts" />
var MUSA;
(function (MUSA) {
    var Controllers;
    (function (Controllers) {
        "use strict";
        var Login = /** @class */ (function () {
            function Login(lock) {
                this.lock = lock;
            }
            return Login;
        }());
        angular.module('musa').controller('LoginController', Login);
    })(Controllers = MUSA.Controllers || (MUSA.Controllers = {}));
})(MUSA || (MUSA = {}));
/// <reference path="../../typings/index.d.ts" />
var MUSA;
(function (MUSA) {
    var Controllers;
    (function (Controllers) {
        "use strict";
        var MACM = /** @class */ (function () {
            function MACM(localStorageService, Backend, HostConfig, $q, $timeout) {
                this.localStorageService = localStorageService;
                this.Backend = Backend;
                this.HostConfig = HostConfig;
                this.$q = $q;
                this.$timeout = $timeout;
                this.GraphUrl = 'graph/query';
            }
            MACM.prototype.fetchMACMCreate = function (applicationID) {
                return this.Backend.fetchExt(this.HostConfig.modellerHost + "eu.musa.modeller.ws/webresources/camelfilews/getCamel/" + applicationID + "/MACM");
            };
            MACM.prototype.getMACMDeleteQuery = function (applicationID) {
                return "MATCH (n)\n                    WHERE n.app_id = \"" + applicationID + "\" \n                    DETACH DELETE n";
            };
            MACM.prototype.getIaaSMachinesQuery = function (applicationID) {
                return "MATCH (n:IaaS)\n                    WHERE n.app_id = " + applicationID + "\n                    RETURN {\n                        name: n.name,\n                        location: n.location\n                    }";
            };
            MACM.prototype.getComponentSecReqQuery = function (applicationID, componentID) {
                return "MATCH (n:service)\n                    WHERE n.app_id = \"" + applicationID + "\"\n                    AND n.component_id = \"" + componentID + "\"\n                    RETURN n.seccap_provided";
            };
            MACM.prototype.getVMForComponentQuery = function (applicationID, componentID) {
                return "MATCH (s:IaaS)-[:hosts*1..2]->(n:service)\n                    WHERE n.app_id = \"" + applicationID + "\"\n                    AND n.component_id = \"" + componentID + "\"\n                    RETURN s";
            };
            MACM.prototype.getComponentsQuery = function (applicationID) {
                return "MATCH (c)\n                    WHERE c.app_id = \"" + applicationID + "\"\n                    AND EXISTS(c.component_id)\n                    AND NOT c.type =~ \"AGENT.*\"\n                    RETURN c";
            };
            MACM.prototype.prepareCspPayload = function (dataToRun) {
                var payload = {
                    "name": dataToRun["name"],
                    "app_id": dataToRun["app_id"].toString()
                };
                var payloadString = JSON.stringify(payload);
                payloadString = payloadString.replace(/\"([^(\")"]+)\":/g, "$1:");
                return payloadString;
            };
            MACM.prototype.getCspDeleteQuery = function (app_id) {
                return "MATCH (csp:CSP)\n                    WHERE csp.app_id = \"" + app_id + "\"\n                    DETACH DELETE csp";
            };
            MACM.prototype.getCspCreateQuery = function (dataToRun) {
                return "CREATE (csp:CSP " + this.prepareCspPayload(dataToRun) + ")";
            };
            MACM.prototype.getCspVmConnectionQuery = function (dataToRun) {
                var queryStringStart = "MATCH (csp:CSP " + this.prepareCspPayload(dataToRun) + ") ";
                var queryStringWhere = "";
                var queryStringEnd = "";
                _.each(dataToRun["vms"], function (vm, k) {
                    vm["app_id"] = vm["app_id"].toString();
                    var joiner = (queryStringWhere == "") ? " WHERE " : " AND ";
                    queryStringStart += " , (vm" + k + ")";
                    queryStringWhere += joiner + ("vm" + k + ".name = \"" + vm["name"] + "\"");
                    queryStringWhere += " AND vm" + k + ".app_id = \"" + vm["app_id"] + "\"";
                    queryStringEnd += " CREATE (csp)-[:provides]->(vm" + k + ")";
                });
                // Remove the quotes on the arguments 
                // return `
                // CREATE (csp:CSP ${cspString})
                // WITH (csp)
                // MATCH (vm:IaaS:service {name:’<vmname>’, app_id:’<appid>’}) CREATE (csp) -[:provides]->(vm)
                //         `;
                // return `MERGE (csp:CSP ${cspString})
                //         MERGE (vm ${vmString})
                //         MERGE (csp)-[:provides]->(vm)`;
                return queryStringStart + queryStringWhere + queryStringEnd;
            };
            MACM.prototype.runGraphQuery = function (query) {
                return this.Backend.update(this.GraphUrl, query);
            };
            MACM.prototype.updateMACM = function (applicationID) {
                var _this = this;
                this.fetchMACMCreate(applicationID)
                    .then(function (fetchResult) {
                    var deleteResult = _this.runGraphQuery(_this.getMACMDeleteQuery(applicationID)).then(function (r) { return r; });
                    return {
                        "fetchResult": fetchResult,
                        "deleteResult": deleteResult
                    };
                }).then(function (results) {
                    _this.runGraphQuery(results["fetchResult"]["data"]).then(function (r) { return results["runNewMacmResult"] = r; });
                    return results;
                })
                    .then(function (ok) {
                    console.log("ok", ok);
                })["catch"](function (err) {
                    console.log('err', err);
                });
            };
            return MACM;
        }());
        angular.module('musa').service('MACM', function (localStorageService, Backend, HostConfig, $q, $timeout) { return new MACM(localStorageService, Backend, HostConfig, $q, $timeout); });
    })(Controllers = MUSA.Controllers || (MUSA.Controllers = {}));
})(MUSA || (MUSA = {}));
/// <reference path="../../typings/index.d.ts" />
var MUSA;
(function (MUSA) {
    var Auth;
    (function (Auth_1) {
        "use strict";
        var Auth = /** @class */ (function () {
            function Auth(lock, localStorageService, jwtHelper) {
                var _this = this;
                this.lock = lock;
                this.localStorageService = localStorageService;
                this.jwtHelper = jwtHelper;
                this.token = this.localStorageService.get('sso.token') || "";
                this.profile = this.localStorageService.get('sso.profile') || "";
                // Listen to authenticated method
                this.lock.on('authenticated', function (authResult) {
                    console.log('auth', authResult);
                    _this.token = authResult.idToken;
                    _this.localStorageService.set('sso.token', _this.token);
                    _this.lock.getProfile(authResult.idToken, function (err, profile) {
                        if (err) {
                            console.log('error Auth0', err);
                        }
                        _this.profile = profile;
                        console.log('profile', _this.profile);
                        _this.localStorageService.set('sso.profile', _this.profile);
                        window.location.reload();
                    });
                });
            }
            Auth.prototype.loginShow = function () {
                return this.lock.show();
            };
            Auth.prototype.logout = function () {
                this.lock.logout();
                this.localStorageService.remove('sso.token');
                this.localStorageService.remove('sso.profile');
                window.location.reload();
            };
            Auth.prototype.isLoggedIn = function () {
                return (this.token != "" && !this.jwtHelper.isTokenExpired(this.token));
            };
            return Auth;
        }());
        angular.module('musa').service('Auth', function (lock, localStorageService, jwtHelper) {
            return new Auth(lock, localStorageService, jwtHelper);
        });
    })(Auth = MUSA.Auth || (MUSA.Auth = {}));
})(MUSA || (MUSA = {}));
/// <reference path="../../typings/index.d.ts" />
var MUSA;
(function (MUSA) {
    var Backend;
    (function (Backend_1) {
        "use strict";
        var Backend = /** @class */ (function () {
            function Backend($http, BackendHost, route, localStorageService) {
                this.$http = $http;
                this.BackendHost = BackendHost;
                this.route = route;
                this.localStorageService = localStorageService;
                this.API = BackendHost.path() + route;
                this.AuthToken = this.localStorageService.get('sso.token') || "noToken";
                this.config = {
                    headers: {
                        'Authorization': this.AuthToken,
                        'UserId': (_.isNull(this.localStorageService.get('sso.profile'))) ? {} : this.localStorageService.get('sso.profile')["user_id"],
                        'Groups': (_.isNull(this.localStorageService.get('sso.profile'))) ? {} : this.localStorageService.get('sso.profile')["groups"]
                    }
                };
            }
            Backend.prototype.url = function () {
                return this.API;
            };
            Backend.prototype.fetch = function (endPoint) {
                return this.$http.get(this.API + endPoint, this.config);
            };
            Backend.prototype.insert = function (endPoint, data) {
                return this.$http.post(this.API + endPoint, data, this.config);
            };
            Backend.prototype.update = function (endPoint, data) {
                return this.$http.put(this.API + endPoint, data, this.config);
            };
            Backend.prototype.remove = function (endPoint) {
                return this.$http["delete"](this.API + endPoint, this.config);
            };
            Backend.prototype.fetchExt = function (endPoint) {
                return this.$http.get(endPoint, this.config);
            };
            Backend.prototype.insertExt = function (endPoint, data) {
                return this.$http.post(endPoint, data, this.config);
            };
            Backend.prototype.updateExt = function (endPoint, data) {
                return this.$http.put(endPoint, data, this.config);
            };
            Backend.prototype.removeExt = function (endPoint) {
                return this.$http["delete"](endPoint, this.config);
            };
            return Backend;
        }());
        angular.module('musa').service('Backend', function ($http, BackendHost, localStorageService) {
            return new Backend($http, BackendHost, '', localStorageService);
        });
    })(Backend = MUSA.Backend || (MUSA.Backend = {}));
})(MUSA || (MUSA = {}));
/// <reference path="../../typings/index.d.ts" />
var MUSA;
(function (MUSA) {
    var Config;
    (function (Config_1) {
        var Config = /** @class */ (function () {
            function Config(localStorageServiceProvider, $stateProvider, $urlRouterProvider, lockProvider, $sceDelegateProvider) {
                this.localStorageServiceProvider = localStorageServiceProvider;
                this.$stateProvider = $stateProvider;
                this.$urlRouterProvider = $urlRouterProvider;
                this.lockProvider = lockProvider;
                this.$sceDelegateProvider = $sceDelegateProvider;
                this.init();
                this.lockProvider.init({
                    clientID: 'QUvgdajNwTBXw9ZyayTRKSelGiiudo0J',
                    domain: 'musa-project.eu.auth0.com',
                    options: {
                        _idTokenVerification: false,
                        auth: {
                            responseType: "token",
                            redirect: false,
                            params: {
                                scope: 'openid email app_metadata'
                            }
                        }
                    }
                });
            }
            Config.prototype.init = function () {
                this.$stateProvider.state('component', Config.component());
                this.$stateProvider.state('riskprofile', Config.riskprofile());
                this.$stateProvider.state('login', Config.login());
                this.$urlRouterProvider.otherwise('/component');
                this.localStorageServiceProvider.setPrefix('musa');
                this.$sceDelegateProvider.resourceUrlWhitelist(['http://*.musa-project.eu/**', 'self']);
            };
            Config.component = function () {
                return {
                    url: '/component',
                    controller: 'ComponentController as k',
                    templateUrl: 'src/component/template/component.html'
                };
            };
            Config.login = function () {
                return {
                    url: '/login',
                    controller: 'LoginController as l',
                    templateUrl: 'src/login/template/login.html'
                };
            };
            Config.riskprofile = function () {
                return {
                    url: '/riskprofile',
                    controller: 'RiskController as r',
                    templateUrl: 'src/riskProfile/template/riskprofile.html'
                };
            };
            return Config;
        }());
        var Run = /** @class */ (function () {
            function Run(editableOptions, editableThemes, lock) {
                this.editableOptions = editableOptions;
                this.editableThemes = editableThemes;
                this.lock = lock;
                this.editableOptions.theme = 'bs3';
                this.editableThemes.buttonClass = 'btn-xs';
                this.lock.interceptHash();
                // this.lock.on('authenticated', (authResult) => {
                //     console.log('auth', authResult);
                //     localStorage.setItem('id_token', authResult.idToken);
                //     this.lock.getProfile(authResult.idToken, (err, profile) => {
                //         if (err) {
                //             console.log('error Auth0', err);
                //         }
                //         localStorage.setItem('profile', JSON.stringify(profile));
                //     });
                // });
            }
            return Run;
        }());
        angular.module('musa').config(function (localStorageServiceProvider, $stateProvider, $urlRouterProvider, lockProvider, $sceDelegateProvider) {
            new Config(localStorageServiceProvider, $stateProvider, $urlRouterProvider, lockProvider, $sceDelegateProvider);
        })
            .run(function (editableOptions, editableThemes, lock) { return new Run(editableOptions, editableThemes, lock); });
    })(Config = MUSA.Config || (MUSA.Config = {}));
})(MUSA || (MUSA = {}));
/// <reference path="../../typings/index.d.ts" />
var MUSA;
(function (MUSA) {
    var Constants;
    (function (Constants) {
        "use strict";
        var Url = /** @class */ (function () {
            function Url(host, port, protocol) {
                if (port === void 0) { port = 80; }
                if (protocol === void 0) { protocol = 'http'; }
                this.host = host;
                this.port = port;
                this.protocol = protocol;
            }
            Url.prototype.path = function () {
                return this.protocol + "://" + this.host + ":" + this.port + "/";
            };
            return Url;
        }());
        angular.module('musa').constant('BackendHost', new Url(window.location.hostname, _.isEmpty(window.location.port) ? 80 : parseInt(window.location.port)));
    })(Constants = MUSA.Constants || (MUSA.Constants = {}));
})(MUSA || (MUSA = {}));
/// <reference path="../../typings/index.d.ts" />
var MUSA;
(function (MUSA) {
    var Services;
    (function (Services) {
        var Session = /** @class */ (function () {
            function Session(localStorageService, HostConfig, Backend) {
                var _this = this;
                this.localStorageService = localStorageService;
                this.HostConfig = HostConfig;
                this.Backend = Backend;
                this.sessionData = (this.localStorageService.get('session') == null || this.localStorageService.get('session') === '' || !this.localStorageService.get('session')["applicationId"]) ?
                    {
                        "applicationName": "New Application",
                        "modelUrl": this.HostConfig.modellerHost,
                        "riskUrl": this.HostConfig.riskHost,
                        "dstUrl": this.HostConfig.dstHost,
                        "deployerUrl": this.HostConfig.deployerHost,
                        "columns": [
                            { "name": "Model", "items": [] },
                            { "name": "Risk", "items": [] },
                            { "name": "Service Selection", "items": [] },
                            { "name": "SLA", "items": [] },
                            { "name": "Plan Deployment", "items": [] },
                            { "name": "Deploy", "items": [] }
                        ]
                    }
                    :
                        this.localStorageService.get('session');
                if (!this.sessionData["applicationId"] || this.sessionData["applicationId"] == 0) {
                    Backend.insert("session", this.sessionData).then(function (response) {
                        _this.sessionData["applicationId"] = response.data;
                        _this.set(_this.sessionData);
                    });
                }
                else {
                    this.refresh();
                }
            }
            Session.prototype.get = function () {
                return this.sessionData;
            };
            Session.prototype.set = function (newSessionData) {
                if (newSessionData != this.sessionData) {
                    angular.copy(newSessionData, this.sessionData);
                }
                this.localStorageService.set('session', this.sessionData);
                this.Backend.update("session/" + this.sessionData["applicationId"], this.sessionData).then(function (response) { return console.log('updated session', response); });
                return this.sessionData;
            };
            Session.prototype.refresh = function () {
                var _this = this;
                this.Backend.fetch("session/" + this.sessionData["applicationId"]).then(function (r) {
                    r.data["applicationId"] = _this.sessionData["applicationId"];
                    r.data["startMonitoringUrl"] = _this.HostConfig.monitorHost + "?operator?start=1&appId=" + r.data["applicationId"] + "&sso=" + _this.localStorageService.get('sso.token');
                    angular.copy(r.data, _this.sessionData);
                    _this.localStorageService.set('session', _this.sessionData);
                });
                return this.sessionData;
            };
            return Session;
        }());
        angular.module('musa').service('Session', function (localStorageService, HostConfig, Backend) {
            return new Session(localStorageService, HostConfig, Backend);
        });
    })(Services = MUSA.Services || (MUSA.Services = {}));
})(MUSA || (MUSA = {}));
/// <reference path="../../typings/index.d.ts" />
var MUSA;
(function (MUSA) {
    var Constants;
    (function (Constants) {
        "use strict";
        var HostConfig = /** @class */ (function () {
            function HostConfig() {
                this.dashboardHost = 'http://framework.musa-project.eu/';
                this.riskHost = this.dashboardHost;
                this.dstHost = this.dashboardHost;
                this.deployerHost = this.dashboardHost + 'deployer/';
                this.slaGeneratorHost = this.dashboardHost + 'sla-generator/';
                this.monitorHost = 'http://assurance-platform.musa-project.eu/';
                this.modellerHost = this.dashboardHost + 'modeller/';
                if (window.location.host.includes("localhost")) {
                    // for dev purposes
                    this.dashboardHost = 'http://localhost:8083/';
                }
                else if (window.location.host.includes("dev")) {
                    // dev deployment   
                    this.deployerHost = this.replaceHost(this.deployerHost, 'framework');
                    this.slaGeneratorHost = this.replaceHost(this.slaGeneratorHost, 'framework');
                    this.modellerHost = this.replaceHost(this.modellerHost, 'framework');
                }
            }
            HostConfig.prototype.replaceHost = function (input, word) {
                return input.replace(word, word + "-dev");
            };
            return HostConfig;
        }());
        angular.module('musa').constant('HostConfig', new HostConfig());
    })(Constants = MUSA.Constants || (MUSA.Constants = {}));
})(MUSA || (MUSA = {}));
/// <reference path="../../typings/index.d.ts" />
var MUSA;
(function (MUSA) {
    var Services;
    (function (Services) {
        var Validation = /** @class */ (function () {
            function Validation(Session, Backend, HostConfig, localStorageService, MACM) {
                this.Session = Session;
                this.Backend = Backend;
                this.HostConfig = HostConfig;
                this.localStorageService = localStorageService;
                this.MACM = MACM;
                this.sessionData = this.Session.get();
                // check monitoring every minute
                setInterval(this.validateMonitor(), 60 * 1000);
            }
            Validation.prototype.updateItemsValidation = function (key, value, itemToUpdate) {
                var _this = this;
                if (itemToUpdate === void 0) { itemToUpdate = "all"; }
                _.each(this.sessionData["columns"], function (column, idx) {
                    _.each(column["items"], function (item, iidx) {
                        if (_.isUndefined(_this.sessionData["columns"][idx]["items"][iidx]["statusOK"])) {
                            _this.sessionData["columns"][idx]["items"][iidx]["statusOK"] = [];
                        }
                        if (itemToUpdate != "all" && item["cid"] == itemToUpdate["cid"]) {
                            _this.sessionData["columns"][idx]["items"][iidx]["statusOK"][key] = value;
                        }
                        else {
                            _this.sessionData["columns"][idx]["items"][iidx]["statusOK"][key] = value;
                        }
                    });
                });
            };
            ;
            Validation.prototype.updateMachineData = function () {
                var _this = this;
                _.each(this.sessionData["columns"], function (column, idx) {
                    _.each(column["items"], function (item, iidx) {
                        if (_.isEmpty(item["deploymentMachine"])) {
                            _this.MACM.runGraphQuery(_this.MACM.getVMForComponentQuery(_this.sessionData["applicationId"], item["cid"])).then(function (r) {
                                return _this.sessionData["columns"][idx]["items"][iidx]["deploymentMachine"] = (!!r["data"][0]) ? r["data"][0]["Values"]["s"]["Properties"] : {};
                            });
                        }
                    });
                });
            };
            Validation.prototype.validateModel = function () {
                var _this = this;
                var url = this.HostConfig.modellerHost + 'eu.musa.modeller.ws/webresources/camelfilews/getValidationErrorsByApplication/' + this.sessionData["applicationId"];
                this.Backend.fetchExt(url).then(function (r) {
                    _this.updateItemsValidation("Model", (!!r.data["ValidationErrorStatus"]) ? r.data["ValidationErrorStatus"] : true);
                });
            };
            Validation.prototype.validateDeployer = function () {
                this.updateItemsValidation("Plan deployment", true);
                this.updateItemsValidation("Deploy", true);
            };
            Validation.prototype.validateServiceSellection = function () {
                var _this = this;
                this.Backend.fetchExt(this.sessionData["getDstUrl"] || this.HostConfig.dstHost + 'serviceselection/' + this.sessionData["applicationId"])
                    .then(function (r) { return _this.updateItemsValidation("Service Selection", true); })["catch"](function (err) { return _this.updateItemsValidation("Service Selection", false); });
            };
            Validation.prototype.validateMonitor = function () {
                var _this = this;
                console.log('monitoring refreshing');
                _.each(this.sessionData["columns"], function (column, idx) {
                    _.each(column["items"], function (item, iidx) {
                        var url = _this.HostConfig.monitorHost + "operator/status?appId="
                            + _this.sessionData["applicationId"] + "&componentId="
                            + item["componentId"] + "&sso="
                            + _this.localStorageService.get('sso.token');
                        _this.Backend.fetchExt(url).then(function (r) {
                            if (!!r.data["status"]) {
                                if (r.data["underRemediation"] != 0 || r.data["alert"] != 0) {
                                    _this.updateItemsValidation("Monitor", "warning", item);
                                }
                                if (r.data["violate"] != 0) {
                                    _this.updateItemsValidation("Monitor", false, item);
                                }
                            }
                        });
                    });
                });
            };
            Validation.prototype.validationMessage = function (columnName) {
                switch (columnName) {
                    case "Model":
                        return "Model Validation errors";
                    case "Risk":
                        return "Risk assessment incomplete";
                    case "Service Selection":
                        return "Service selection incomplete";
                    case "Plan Deployment":
                    case "Deploy":
                        return "Deployment plan errors";
                    case "Sla":
                    case "SLA":
                        return "SLA generation errors";
                    case "Monitor":
                        return "Some errors detected by the monitoring";
                    default:
                        return "Step errors!";
                }
            };
            Validation.prototype.updateAllValidations = function () {
                this.validateModel();
                this.validateServiceSellection();
                this.validateDeployer();
                this.updateMachineData();
            };
            return Validation;
        }());
        angular.module('musa').service('Validation', function (Session, Backend, HostConfig, localStorageService, MACM) { return new Validation(Session, Backend, HostConfig, localStorageService, MACM); });
    })(Services = MUSA.Services || (MUSA.Services = {}));
})(MUSA || (MUSA = {}));
/// <reference path="../../typings/index.d.ts" />
var MUSA;
(function (MUSA) {
    var Controllers;
    (function (Controllers) {
        "use strict";
        var Risk = /** @class */ (function () {
            function Risk(localStorageService, $uibModal, $window, Backend, HostConfig, $location, Session, Validation) {
                var _this = this;
                this.localStorageService = localStorageService;
                this.$uibModal = $uibModal;
                this.$window = $window;
                this.Backend = Backend;
                this.HostConfig = HostConfig;
                this.$location = $location;
                this.Session = Session;
                this.Validation = Validation;
                this.riskProfile = this.localStorageService.get('riskProfile') || [];
                this.currentSession = this.localStorageService.get('session');
                console.log(this.currentSession);
                this.routeSelectedComponent = this.$location.search()["component"];
                if (_.isEmpty(this.riskProfile)) {
                    this.Backend.fetch("riskprofile/" + this.currentSession["applicationId"]).then(function (response) {
                        _this.riskProfile = response.data;
                        _this.updateComponents();
                        _this.updateRisks();
                    }, function (error) {
                        _this.riskProfile = [
                            { "name": "Components Description", "items": [] },
                            { "name": "STRIDE based Risk Assessment", "items": [] },
                            { "name": "Security Controls", "items": [] },
                            { "name": "Validation", "items": [] }
                        ];
                        _this.updateComponents();
                        _this.localStorageService.set('riskProfile', _this.riskProfile);
                        _this.Backend.insert("riskprofile/" + _this.currentSession["applicationId"], _this.riskProfile);
                    });
                }
                else {
                    this.Backend.fetch("riskprofile/" + this.currentSession["applicationId"]).then(function (response) {
                        _this.riskProfile = response.data;
                        _this.updateComponents();
                        _this.updateRisks();
                    });
                }
            }
            Risk.prototype.orderByStride = function (i) { return ["SPOOFING", "TAMPERING", "REPUDIATION", "INFORMATION DISCLOSURE", "DENIAL OF SERVICE", "ELEVATION OF PRIVILEGES"].indexOf(i["name"]); };
            ;
            Risk.prototype.updateComponents = function () {
                var _this = this;
                var currentComponents = _.flatten(_.map(_.slice(this.currentSession["columns"], _.findIndex(this.currentSession["columns"], function (v) { return v["name"] == "Risk"; })), function (v) { return v["items"]; }));
                var currentRiskComponents = _.flatten(_.map(this.riskProfile, function (v) { return v["items"]; }));
                // Add components from the current session
                _.each(currentComponents, function (component) {
                    if (_.findIndex(currentRiskComponents, function (v) { return v["cid"] == component["cid"]; }) == -1) {
                        _this.riskProfile[0]["items"].push(component);
                    }
                });
                // Remove components if they do not exist anymore
                // go over each riskProfile column
                _.each(this.riskProfile, function (riskColumn, riskColumnIndex) {
                    riskColumn["items"].forEach(function (item, itemsIndex) {
                        if (_.findIndex(currentComponents, function (v) { return v["cid"] == item["cid"]; }) == -1) {
                            _this.riskProfile[riskColumnIndex]["items"].splice(itemsIndex, 1);
                        }
                    });
                });
            };
            Risk.prototype.clearSession = function () {
                this.riskProfile = [];
                this.localStorageService.clearAll();
                window.location.reload();
            };
            Risk.prototype.updateRisks = function () {
                this.localStorageService.set('riskProfile', this.riskProfile);
                this.Backend.update("riskprofile/" + this.currentSession["applicationId"], this.riskProfile).then(function (r) { return console.log("riskProfile updated", r); });
                this.currentSession["getRiskUrl"] = this.Backend.url() + "riskprofile/" + this.currentSession["applicationId"];
                this.currentSession["RiskTS"] = new Date();
                this.Session.set(this.currentSession);
            };
            Risk.prototype.updateSession = function () {
                this.Session.set(this.currentSession);
            };
            Risk.prototype.updateComponent = function (componentData) {
                var data = componentData;
                delete data["OWASPModel"];
                this.Backend.update("riskprofile/" + this.currentSession["applicationId"] + "/component/" + componentData["cid"], data);
            };
            Risk.prototype.countThreats = function (item) {
                var _this = this;
                var getPositionOfAnItemInASession = function (item) {
                    var pos = [];
                    _.each(_this.currentSession["columns"], function (column, index) {
                        var location = _.findIndex(column["items"], function (sessionItem) { return sessionItem["cid"] == item["cid"]; });
                        if (location != -1) {
                            pos.push(index);
                            pos.push(location);
                        }
                    });
                    return pos;
                };
                var changedItemToProfiled = function (item) {
                    var pos = getPositionOfAnItemInASession(item);
                    _this.currentSession["columns"][pos[0]]["items"][pos[1]]["status"] = "PROFILED";
                    //this.Session.set(this.currentSession);
                };
                if (!!item.risks) {
                    var totalRisks = item.risks.length;
                    var risksResolved = _(item.risks)
                        .filter(function (r) { return r["roam"] == "Resolved"; })
                        .value()
                        .length;
                    var risksAddressed = _(item.risks)
                        .filter(function (r) { return (r["roam"] == "Accepted" || r["roam"] == "Mitigated"); })
                        .value()
                        .length;
                    var count = (totalRisks - risksResolved == risksAddressed) ? -1 : (totalRisks - risksResolved - risksAddressed);
                    if (count == -1) {
                        changedItemToProfiled(item);
                        item["status"] = "PROFILED";
                        // Set the validationStatus to OK
                        this.Validation.updateItemsValidation("Risk", true, item);
                    }
                    else {
                        this.Validation.updateItemsValidation("Risk", false, item);
                    }
                    return count;
                }
                else {
                    return 0;
                }
            };
            Risk.prototype.countThreatsMessage = function (item) {
                var count = this.countThreats(item);
                return (count == 0) ? "Analysis Needed!" : (count == 1) ? "Analysis required for " + count + " threat" : "Analysis required for " + count + " threats";
            };
            Risk.prototype.itemRiskSeverity = function (item, level) {
                var totalLC = 0;
                if (!!item.risks) {
                    item.risks.forEach(function (LC) { return totalLC += ((LC["likelihood"] || 0) + (LC["impact"] || 0)) * 0.125; });
                    var sumLC = totalLC / item.risks.length;
                    var result = false;
                    switch (level) {
                        case 'low':
                            result = (sumLC < 6);
                            break;
                        case 'medium':
                            result = (sumLC < 12 && sumLC >= 6);
                            break;
                        case 'high':
                            result = (sumLC >= 12);
                            break;
                    }
                }
                else {
                    result = level == 'no';
                }
                return result;
            };
            Risk.prototype.itemMitigationLevel = function (item, level) {
                var totalSR = 0;
                if (!!item.risks) {
                    item.risks.forEach(function (s) { return totalSR += !!s["sR"] ? s["sR"].length : 0; });
                    var itemSR = totalSR / item.risks.length;
                    var result = false;
                    switch (level) {
                        case 'low':
                            result = (itemSR >= 2);
                            break;
                        case 'medium':
                            result = (itemSR > 0 && itemSR < 2);
                            break;
                        case 'high':
                            result = (itemSR == 0);
                            break;
                    }
                    return result;
                }
                else {
                    return level == 'no';
                }
            };
            Risk.prototype.dragOptions = function () {
                var _this = this;
                return {
                    dragEnd: function (e) {
                        _this.updateComponent(e['source']['itemScope']['item']);
                        _this.updateRisks();
                    }
                };
            };
            Risk.prototype.showDetails = function (item, columnName) {
                var _this = this;
                var tpl = columnName
                    .split(" ")
                    .map(function (w) { return w.substring(0, 3).toLowerCase(); })
                    .slice(0, 2)
                    .join("");
                this.$uibModal.open({
                    templateUrl: 'modal_templates/' + tpl + '.html',
                    size: 'lg',
                    controller: function ($uibModalInstance, item, localStorageService, Backend) {
                        var _this = this;
                        this.item = item;
                        this.localStorageService = localStorageService;
                        this.Backend = Backend;
                        this.threats = [];
                        this.strideCategories = [];
                        this.securityControls = [];
                        this.suggestedControls = [];
                        this.fetchData = function (url, placeholder) { return _this.Backend.fetch(url).then(function (r) { return _this[placeholder] = r.data; }); };
                        this.certificates =
                            [
                                { "id": "ISO", "name": "ISO" },
                                { "id": "Nist", "name": "Nist" },
                                { "id": "Cobit", "name": "Cobit" },
                                { "id": "Hippa", "name": "Hippa" }
                            ];
                        this.orderByStride = function (i) { return ["SPOOFING", "TAMPERING", "REPUDIATION", "INFORMATION DISCLOSURE", "DENIAL OF SERVICE", "ELEVATION OF PRIVILEGES"].indexOf(i["name"]); };
                        this.OWASPModel =
                            [
                                {
                                    "children": [
                                        {
                                            "children": [
                                                {
                                                    "description": " How technically skilled is this group of threat agents? Security penetration skills (9), network and programming skills (6), advanced computer user (5), some technical skills (3), no technical skills (1)",
                                                    "name": "Skill level",
                                                    "id": "skillLevel"
                                                },
                                                {
                                                    "description": " How motivated is this group of threat agents to find and exploit this vulnerability? Low or no reward (1), possible reward (4), high reward (9)",
                                                    "name": "Motive",
                                                    "id": "motive"
                                                },
                                                {
                                                    "description": " What resources and opportunities are required for this group of threat agents to find and exploit this vulnerability? Full access or expensive resources required (0), special access or resources required (4), some access or resources required (7), no access or resources required (9)",
                                                    "name": "Opportunity",
                                                    "id": "opportunity"
                                                },
                                                {
                                                    "description": " How large is this group of threat agents? Developers (2), system administrators (2), intranet users (4), partners (5), authenticated users (6), anonymous Internet users (9)",
                                                    "name": "Size",
                                                    "id": "size"
                                                }
                                            ],
                                            "description": "The first set of factors are related to the threat agent involved. The goal here is to estimate the likelihood of a successful attack by this group of threat agents. Use the worst-case threat agent.",
                                            "name": "Threat Agent Factory"
                                        },
                                        {
                                            "children": [
                                                {
                                                    "description": " How easy is it for this group of threat agents to discover this vulnerability? Practically impossible (1), difficult (3), easy (7), automated tools available (9)",
                                                    "name": "Ease of discovery",
                                                    "id": "easeOfDiscovery"
                                                },
                                                {
                                                    "description": " How easy is it for this group of threat agents to actually exploit this vulnerability? Theoretical (1), difficult (3), easy (5), automated tools available (9)",
                                                    "name": "Ease of exploit",
                                                    "id": "easeOfExploit"
                                                },
                                                {
                                                    "description": " How well known is this vulnerability to this group of threat agents? Unknown (1), hidden (4), obvious (6), public knowledge (9)",
                                                    "name": "Awareness",
                                                    "id": "awareness"
                                                },
                                                {
                                                    "description": " How likely is an exploit to be detected? Active detection in application (1), logged and reviewed (3), logged without review (8), not logged (9)",
                                                    "name": "Intrusion detection",
                                                    "id": "intrusionDetection"
                                                }
                                            ],
                                            "description": "The next set of factors are related to the vulnerability involved. The goal here is to estimate the likelihood of the particular vulnerability involved being discovered and exploited. Assume the threat agent selected above.",
                                            "name": "Vulnerability Factors"
                                        }
                                    ],
                                    "description": "At the highest level, this is a rough measure of how likely this particular vulnerability is to be uncovered and exploited by an attacker.",
                                    "name": "likelihood"
                                },
                                {
                                    "children": [
                                        {
                                            "children": [
                                                {
                                                    "description": " How much data could be disclosed and how sensitive is it? Minimal non-sensitive data disclosed (2), minimal critical data disclosed (6), extensive non-sensitive data disclosed (6), extensive critical data disclosed (7), all data disclosed (9)",
                                                    "name": "Loss of confidentiality",
                                                    "id": "lossOfConfidentiality"
                                                },
                                                {
                                                    "description": " How much data could be corrupted and how damaged is it? Minimal slightly corrupt data (1), minimal seriously corrupt data (3), extensive slightly corrupt data (5), extensive seriously corrupt data (7), all data totally corrupt (9)",
                                                    "name": "Loss of integrity",
                                                    "id": "lossOfIntegrity"
                                                },
                                                {
                                                    "description": " How much service could be lost and how vital is it? Minimal secondary services interrupted (1), minimal primary services interrupted (5), extensive secondary services interrupted (5), extensive primary services interrupted (7), all services completely lost (9)",
                                                    "name": "Loss of availability",
                                                    "id": "lossOfAvailability"
                                                },
                                                {
                                                    "description": " Are the threat agents' actions traceable to an individual? Fully traceable (1), possibly traceable (7), completely anonymous (9)",
                                                    "name": "Loss of accountability",
                                                    "id": "lossOfAccountability"
                                                }
                                            ],
                                            "description": "Technical impact can be broken down into factors aligned with the traditional security areas of concern: confidentiality, integrity, availability, and accountability. The goal is to estimate the magnitude of the impact on the system if the vulnerability were to be exploited.",
                                            "name": "Technical Impact Factors"
                                        },
                                        {
                                            "children": [
                                                {
                                                    "description": " How much financial damage will result from an exploit? Less than the cost to fix the vulnerability (1), minor effect on annual profit (3), significant effect on annual profit (7), bankruptcy (9)",
                                                    "name": "Financial damage",
                                                    "id": "financialDamage"
                                                },
                                                {
                                                    "description": " Would an exploit result in reputation damage that would harm the business? Minimal damage (1), Loss of major accounts (4), loss of goodwill (5), brand damage (9)",
                                                    "name": "Reputation damage",
                                                    "id": "reputationDamage"
                                                },
                                                {
                                                    "description": " How much exposure does non-compliance introduce? Minor violation (2), clear violation (5), high profile violation (7)",
                                                    "name": "Non-compliance",
                                                    "id": "nonCompliance"
                                                },
                                                {
                                                    "description": " How much personally identifiable information could be disclosed? One individual (3), hundreds of people (5), thousands of people (7), millions of people (9)",
                                                    "name": "Privacy violation",
                                                    "id": "privacyViolation"
                                                }
                                            ],
                                            "description": " The business impact stems from the technical impact, but requires a deep understanding of what is important to the company running the application. In general, you should be aiming to support your risks with business impact, particularly if your audience is executive level. The business risk is what justifies investment in fixing security problems.",
                                            "name": "Business Impact Factors"
                                        }
                                    ],
                                    "description": "When considering the impact of a successful attack, it's important to realize that there are two kinds of impacts. The first is the \"technical impact\" on the application, the data it uses, and the functions it provides. The other is the \"business impact\" on the business and company operating the application.",
                                    "name": "impact"
                                }
                            ];
                        this.item.sRA = !!this.item.sRA ? this.item.sRA : [];
                        this.item.OWASPModel = !!this.item.OWASPModel ? this.item.OWASPModel : this.OWASPModel;
                        this.close = function () {
                            $uibModalInstance.close();
                        };
                        this.showOnlySuggested = true;
                        this.showDescriptions = true;
                        this.pushToArray = function (arr, object, key, keyValue) {
                            if (key === void 0) { key = null; }
                            if (keyValue === void 0) { keyValue = ''; }
                            // if (arr.indexOf(object) == -1) {
                            if (_.isNull(key)) {
                                if (_.indexOf(arr, object) == -1) {
                                    arr.push(object);
                                }
                            }
                            else {
                                if (_.isEmpty(_.find(arr, function (i) { return i[key] == keyValue; }))) {
                                    arr.push(object);
                                }
                            }
                        };
                        this.logStat = function () {
                            var data = [];
                            _.each(arguments, function (i) {
                                data.push(i);
                                data.push("<===");
                            });
                            _this.Backend.update("riskprofile/log", JSON.stringify(data, null, '\t'));
                        };
                        this.fetchCcmControls = function (component, nistControl) {
                            _this.Backend.fetch("nist/" + nistControl['id'] + "/ccm").then(function (r) {
                                if (!!component.ccm) {
                                    _.each(r.data, function (v) { return _this.pushToArray(component.ccm, v); });
                                }
                                else {
                                    component.ccm = r.data;
                                }
                                // TEMP: double store for the mapping back
                                if (!component.ccmMap) {
                                    component.ccmMap = [];
                                }
                                component.ccmMap.push({
                                    "nist": nistControl["id"],
                                    "ccm": r.data
                                });
                            });
                            console.log('component', component);
                        };
                        this.onThreatChange = function () {
                            _this.suggestedControls = [];
                            _this.Backend.fetch('slaeditor/threat/' + _this.addToRisk['id'] + '/controls').then(function (r) {
                                _this.suggestedControls = r.data.map(function (v) { return v['id']; });
                            });
                        };
                        this.securityControlFilter = function (item) {
                            if (_this.showOnlySuggested && !!_this.addToRisk) {
                                return (_this.suggestedControls.indexOf(item['id']) > -1);
                            }
                            else {
                                return true;
                            }
                        };
                        this.addRisk = function () {
                            var risks = item.risks || [];
                            risks.push({
                                "name": "",
                                "probability": 1,
                                "impact": 1
                            });
                            item.risks = risks;
                        };
                        this.calculateRiskSeverity = function (riskName, level) {
                            // filter our the risk
                            var risk = this.item.risks.filter(function (v) { return v.name == riskName; });
                            // calculate
                            var LC = _.filter(risk, function (v) { return v["name"] == riskName; })[0];
                            // return true or false depending on the level
                            var sumLC = ((LC["likelihood"] || 0) + (LC["impact"] || 0)) * 0.125;
                            var result = false;
                            switch (level) {
                                case 'low':
                                    result = (sumLC < 6);
                                    break;
                                case 'medium':
                                    result = (sumLC < 12 && sumLC >= 6);
                                    break;
                                case 'high':
                                    result = (sumLC >= 12);
                                    break;
                            }
                            return result;
                        };
                        this.removeItem = function (array, risk) {
                            if (_.indexOf(array, risk) != -1) {
                                array.splice(_.indexOf(array, risk), 1);
                            }
                        };
                        this.compileCSAControls = function () {
                            var _this = this;
                            // cleanup the item to extract the parents
                            // remove duplicates
                            var SRI = _(this.item["risks"])
                                .map(function (o) { return o["sR"]; })
                                .flatten()
                                .extend(this.item["sRA"])
                                .uniqBy(function (e) { return e.id; })
                                .value();
                            var sR = _.uniqBy(_.extend(_.flatten(_.map(this.item["risks"], function (o) { return o["sR"]; })), this.item["sRA"]), function (e) { return e['id']; });
                            console.log("SRI", SRI, "sR", sR);
                            sR = _.each(sR, function (v) {
                                v['children'] = [];
                                var description = v['name'];
                                v['name'] = v['id'];
                                v['description'] = description;
                                v['children'].push(_this.CSAMetrics[Math.floor(Math.random() * _this.CSAMetrics.length)]);
                            });
                            var data = {
                                name: 'ROOT',
                                id: 'root',
                                children: sR
                            };
                            return data;
                        };
                        this.sumOWASP = function () {
                            var _this = this;
                            // for each of the 1st level OWASP dimensions
                            _.each(this.item.OWASPModel, function (LI) {
                                // for each of the risk
                                _.each(_this.item.risks, function (risk) {
                                    // extract children
                                    var group = _this.item.OWASPModel.filter(function (v) { return v.name == LI.name; });
                                    var groupValues = _.flatMap(group[0].children, function (v) { return v.children.map(function (v1) { return v1.id; }); });
                                    // cleanup values
                                    _.remove(groupValues, _.isUndefined);
                                    // filter user values from OWASP model
                                    var userSetValues = _.pickBy(risk["owasp"], function (value, key) {
                                        return _.includes(groupValues, key);
                                    });
                                    // calculate probability and impact value
                                    var indexOfRisk = _.findIndex(_this.item.risks, function (v) { return v["name"] == risk.name; });
                                    _this.item.risks[indexOfRisk][LI.name] = _(userSetValues).values().sum();
                                });
                            });
                        };
                        this.slider = {
                            options: {
                                floor: 0,
                                ceil: 9,
                                step: 1,
                                showTicks: true,
                                showSelectionBar: true,
                                getSelectionBarColor: function (value) {
                                    if (value <= 3)
                                        return 'green';
                                    if (value <= 6)
                                        return 'orange';
                                    if (value <= 9)
                                        return 'red';
                                    return '#2AE02A';
                                },
                                getPointerColor: function (value) {
                                    if (value <= 3)
                                        return 'green';
                                    if (value <= 6)
                                        return 'orange';
                                    if (value <= 9)
                                        return 'red';
                                    return '#2AE02A';
                                },
                                stepsArray: [
                                    { value: 0, legend: 'Very Unlikely' },
                                    { value: 1 },
                                    { value: 2 },
                                    { value: 3 },
                                    { value: 4 },
                                    { value: 5 },
                                    { value: 6 },
                                    { value: 7 },
                                    { value: 8 },
                                    { value: 9, legend: 'Very Likely' }
                                ],
                                onChange: function () { return _this.sumOWASP(); }
                            }
                        };
                    },
                    controllerAs: 'm',
                    resolve: {
                        item: function () { return item; }
                    }
                }).closed.then(function () {
                    _this.updateRisks();
                    _this.updateSession();
                });
            };
            return Risk;
        }());
        angular.module('musa').controller('RiskController', Risk);
    })(Controllers = MUSA.Controllers || (MUSA.Controllers = {}));
})(MUSA || (MUSA = {}));
/// <reference path="../../typings/index.d.ts" />
var MUSA;
(function (MUSA) {
    var Directive;
    (function (Directive) {
        "use strict";
        function BarChartDirective() {
            function link(scope, element, attr) {
                // specify graph defaults with fallbacks
                var graph = {
                    width: attr.width || 1000,
                    height: attr.height || 900
                };
                var data = scope.data;
                // colouring function
                // var colors = d3.scale.category20();
                var colors = d3.scale.linear()
                    .domain([0, 50, 100])
                    .range(["#c0392b", "#f1c40f", "#27ae60"]);
                var chart = element[0], axisMargin = 20, margin = 20, valueMargin = 4, width = chart.offsetWidth, height = chart.offsetHeight, barHeight = (graph.height - axisMargin - margin * 2) * 0.4 / data.length, barPadding = (graph.height - axisMargin - margin * 2) * 0.6 / data.length, data, bar, svg, scale, xAxis, labelWidth = 0;
                var div = d3.select("body").append("div")
                    .attr("class", "d3tooltip")
                    .style("opacity", 0);
                svg = d3.select(chart)
                    .append("svg")
                    .attr("width", graph.width)
                    .attr("height", graph.height);
                bar = svg.selectAll("g")
                    .data(data)
                    .enter()
                    .append("g");
                bar.attr("class", "bar")
                    .attr("cx", 0)
                    .attr("transform", function (d, i) {
                    return "translate(" + margin + "," + (i * (barHeight + barPadding) + barPadding) + ")";
                });
                bar.append("text")
                    .attr("class", "label")
                    .attr("y", barHeight / 2)
                    .attr("dy", ".35em") //vertical align middle
                    .text(function (d) {
                    return d["SlaId"];
                }).each(function () {
                    labelWidth = Math.ceil(Math.max(labelWidth, this.getBBox().width));
                })
                    .on("mouseover", function (d) {
                    div.transition()
                        .duration(200)
                        .style("opacity", .95);
                    div.html(d["description"] || "")
                        .style("left", (d3.event.pageX + 10) + "px")
                        .style("top", (d3.event.pageY + 10) + "px");
                })
                    .on("mouseout", function (d) {
                    div.transition()
                        .duration(500)
                        .style("opacity", 0);
                });
                scale = d3.scale.linear()
                    .domain([0, 100])
                    .range([0, width - margin * 2 - labelWidth]);
                xAxis = d3.svg.axis()
                    .scale(scale)
                    .tickSize(-height + 2 * margin + axisMargin)
                    .orient("bottom");
                bar.append("rect")
                    .attr("transform", "translate(" + labelWidth + ", 0)")
                    .attr("height", barHeight)
                    .attr('fill', function (d) {
                    return colors(d["Value"] * 100);
                })
                    .attr("width", function (d) {
                    return d["Value"] * 100;
                });
                bar.append("text")
                    .attr("class", "value")
                    .attr("y", barHeight / 2)
                    .attr("dx", +labelWidth) //margin right
                    .attr("dy", ".35em") //vertical align middle
                    .attr("text-anchor", "end")
                    .text(function (d) {
                    return Math.round(d["Value"] * 100).toString() + "%";
                })
                    .attr("x", function (d) {
                    var width = this.getBBox().width;
                    var r = Math.max(width + valueMargin, d["Value"] * 100);
                    return (d["Value"] > 10) ? r + 25 : r;
                });
            }
            return {
                restrict: 'E',
                scope: {
                    data: '='
                },
                link: link
            };
        }
        angular.module('musa').directive("barChart", BarChartDirective);
    })(Directive = MUSA.Directive || (MUSA.Directive = {}));
})(MUSA || (MUSA = {}));
/// <reference path="../../typings/index.d.ts" />
var MUSA;
(function (MUSA) {
    var Controllers;
    (function (Controllers) {
        "use strict";
        var ServiceSelection = /** @class */ (function () {
            function ServiceSelection(localStorageService, Backend, HostConfig, MACM, Session) {
                var _this = this;
                this.localStorageService = localStorageService;
                this.Backend = Backend;
                this.HostConfig = HostConfig;
                this.MACM = MACM;
                this.Session = Session;
                // Init
                this.deployments = [];
                this.selected = [];
                this.riskProfile = this.localStorageService.get('riskProfile') || [];
                this.session = this.Session.get();
                this.lastSelection = this.localStorageService.get('serviceSelection_' + this.session["applicationId"]) || {};
                this.controls = [];
                this.Backend.fetch('slaeditor/control').then(function (r) { return _this.controls = r.data; });
                // Fetch components
                this.components = _.flatten(_.map(_.slice(this.localStorageService.get('session')["columns"], _.findIndex(this.localStorageService.get('session')["columns"], function (v) { return v["name"] == "Service Selection"; })), function (v) { return v["items"]; }));
                this.vms = _.groupBy(this.components, function (c) { return c["deploymentMachine"]["name"]; });
                // Fetch Services
                this.Backend.fetch("csp").then(function (response) {
                    var services = response.data.map(function (v) {
                        var o = {};
                        o["data"] = v;
                        // generate alignment for the service
                        var allAlignment = _this.calculateAlignment(v);
                        var selectedSR = _this.getSelectedSecurityControlIds(_this.riskProfile);
                        // var limitedAlignment = allAlignment.filter(v => {
                        //     return selectedSR.indexOf(v["SlaId"]) > -1
                        // });
                        o["ccmAlignment"] = allAlignment;
                        return o;
                    });
                    var possibleDeployments = [];
                    _.forOwn(_this.vms, function (c, k) {
                        var singleComponentDeployment = [];
                        _.each(services, function (s) {
                            singleComponentDeployment.push({
                                "component": c,
                                "service": s,
                                "vmName": k
                            });
                        });
                        possibleDeployments.push(singleComponentDeployment);
                    });
                    var permutations = _this.cartesianProduct(possibleDeployments);
                    // Calculate the scores
                    _this.deployments = permutations.map(function (singleDeployment) {
                        var o = {};
                        console.log(singleDeployment);
                        var deploymentValues = _(singleDeployment)
                            .map(function (singleCoponentOnSingleService) { return _this.getSecurityControlForComponents(singleCoponentOnSingleService["service"]["ccmAlignment"], singleCoponentOnSingleService["component"]); })
                            .flatten()
                            .map(function (values) { return values["Value"]; })
                            .value();
                        o["score"] = _.ceil(_(deploymentValues).sum() * 100 / deploymentValues.length, 2);
                        if (_.isNaN(o["score"])) {
                            o["score"] = 0;
                        }
                        o["data"] = singleDeployment;
                        return o;
                    });
                });
            }
            ServiceSelection.prototype.cartesianProduct = function (arg) {
                var r = [], max = arg.length - 1;
                function helper(arr, i) {
                    for (var j = 0, l = arg[i].length; j < l; j++) {
                        var a = arr.slice(0); // clone arr
                        a.push(arg[i][j]);
                        if (i == max) {
                            r.push(a);
                        }
                        else
                            helper(a, i + 1);
                    }
                }
                helper([], 0);
                return r;
            };
            //
            ServiceSelection.prototype.calculateAlignment = function (service) {
                var groupAlignment = _(service["Metrics"])
                    .map(function (v) {
                    var i = {};
                    i["group"] = v["SlaId"].split(".")[0];
                    i["value"] = v["Value"];
                    return i;
                })
                    .groupBy("group")
                    .map(function (v, k) {
                    var o = {};
                    o["SlaId"] = k;
                    o["Value"] = _.sumBy(v, "value") / v.length;
                    return o;
                })
                    .value();
                _.each(groupAlignment, function (v) { return service["Metrics"].push(v); });
                return service["Metrics"];
            };
            ServiceSelection.prototype.getSecurityControlsByComponent = function (riskProfile, key) {
                if (key === void 0) { key = 'ccm'; }
                return _(riskProfile)
                    .map(function (v) { return v['items']; })
                    .flatten()
                    .map(function (r) {
                    return {
                        "cid": r['cid'],
                        "ccm": (!!r['risks']) ?
                            _(r['risks'])
                                .map(function (c) { return c[key]; })
                                .flatten()
                                .value()
                            :
                                []
                    };
                })
                    .value();
            };
            ServiceSelection.prototype.getSecurityControlForComponents = function (allAlignment, componentsList) {
                var _this = this;
                return _(componentsList)
                    .map(function (ss) { return _this.getSecurityControlForComponent(allAlignment, ss["cid"]); })
                    .flatten()
                    .value();
            };
            ServiceSelection.prototype.getSecurityControlForComponent = function (allAlignment, componentId) {
                var _this = this;
                var componentControls = _(this.getSecurityControlsByComponent(this.riskProfile))
                    .find(function (v) { return v['cid'] == componentId; });
                var componentControlMap = _(this.getSecurityControlsByComponent(this.riskProfile, 'ccmMap'))
                    .find(function (v) { return v['cid'] == componentId; });
                return (!_.isUndefined(componentControlMap) && !!componentControlMap["ccm"]) ?
                    _(componentControlMap["ccm"])
                        .map(function (c) {
                        return (!!c) ? {
                            "SlaId": c["nist"],
                            "Value": _.ceil(_(allAlignment)
                                .filter(function (singleAlignment) { return c["ccm"].indexOf(singleAlignment["SlaId"]) > -1; })
                                .sumBy(function (v) { return v["Value"]; })
                                / c["ccm"].length, 2),
                            "description": _this.controls[_.findIndex(_this.controls, function (o) { return o["id"] == c["nist"]; })]["description"]
                        }
                            : {};
                    })
                        .uniqBy('SlaId')
                        .compact()
                        .value()
                    : {};
            };
            ServiceSelection.prototype.getSelectedSecurityControlIds = function (riskProfile) {
                return _(riskProfile)
                    .map(function (v) { return v['items']; })
                    .flatten()
                    .map(function (v) { return v['risks']; })
                    .flatten()
                    .compact()
                    .map(function (v) { return v['ccm']; })
                    .flatten()
                    .compact()
                    .uniq()
                    .value();
            };
            ServiceSelection.prototype.saveSelectedDeployment = function (deployment) {
                var _this = this;
                var app_id = this.localStorageService.get('session')["applicationId"];
                // Save it to localStorage
                this.localStorageService.set('serviceSelection_' + app_id, deployment);
                if (!!app_id) {
                    console.log('deployment', deployment);
                    this.Backend.update("serviceselection/" + app_id, deployment)
                        .then(function (r) {
                        console.log("serviceselection updated", r);
                        var sessionData = _this.Session.get();
                        sessionData["dstUrl"] = _this.HostConfig.dstHost;
                        sessionData["getDstUrl"] = _this.Backend.url() + 'serviceselection/' + sessionData["applicationId"];
                        sessionData["DstTS"] = new Date();
                        _this.Session.set(sessionData);
                    });
                    // store it into the graph database as well
                    this.MACM.runGraphQuery(this.MACM.getCspDeleteQuery(app_id))
                        .then(function (deleteResponse) {
                        var responses = {};
                        var dataToRun = [];
                        _.each(deployment.data, function (componentService, index) {
                            console.log('c', componentService);
                            var cspData = {
                                "name": componentService["service"]["data"]["Name"],
                                "app_id": app_id
                            };
                            var vmData = {
                                "name": componentService["vmName"],
                                "app_id": app_id
                            };
                            var idx = _.findIndex(dataToRun, function (csp) { return csp["name"] == cspData["name"]; });
                            if (idx != -1) {
                                dataToRun[idx]["vms"].push(vmData);
                            }
                            else {
                                cspData["vms"] = [];
                                cspData["vms"].push(vmData);
                                dataToRun = dataToRun.concat([cspData]);
                            }
                        });
                        console.log('dataToRun', dataToRun);
                        return [responses, dataToRun];
                    })
                        .then(function (_a) {
                        var responses = _a[0], dataToRun = _a[1];
                        _.each(dataToRun, function (cspPayload) {
                            _this.MACM.runGraphQuery(_this.MACM.getCspCreateQuery(cspPayload)).then(function (response) { return responses[cspPayload["name"] + '-create'] = response; });
                        });
                        return [responses, dataToRun];
                    })
                        .then(function (_a) {
                        var responses = _a[0], dataToRun = _a[1];
                        _.each(dataToRun, function (cspPayload) {
                            _this.MACM.runGraphQuery(_this.MACM.getCspVmConnectionQuery(cspPayload)).then(function (response) { return responses[cspPayload["name"] + '-join'] = response; });
                        });
                        return responses;
                    })
                        .then(function (r) { return console.log('ok', r); })["catch"](function (err) { return console.log('err', err); });
                }
            };
            ServiceSelection.prototype.noResults = function (data) {
                return _.isEmpty(data[0]);
            };
            ServiceSelection.prototype.noLastSelection = function () {
                return _.isEmpty(this.lastSelection);
            };
            return ServiceSelection;
        }());
        angular.module('musa').controller('ServiceSelectionController', ServiceSelection);
    })(Controllers = MUSA.Controllers || (MUSA.Controllers = {}));
})(MUSA || (MUSA = {}));

# MUSA Front End

This is a repository for MUSA front-end interace merging all the tools in a unified process. 

The main objective of MUSA, MUlti-cloud Secure Applications,  is to support the security-intelligent lifecycle management of distributed applications over heterogeneous cloud resources, through a security framework that includes:
* security-by-design mechanisms to allow application self-protection at runtime, and
* methods and tools for the integrated security assurance in both the engineering and operation of multi-cloud applications.

The MUSA framework leverages security-by-design, agile and DevOps approaches in multi-cloud applications, and enables the security-aware development and operation of multi-cloud applications. The framework will be composed of
* an IDE for creating the multi-cloud application taking into account its security requirements together with functional and business requirements,
* a set of security mechanisms embedded in the multi-cloud application components for self-protection,
*  an automated deployment environment that, based on an intelligent decision support system, will allow for the dynamic distribution of the components according to security needs, and
*  a security assurance platform in form of a SaaS that will support multi-cloud application runtime security control and transparency to increase user trust.

The project will demonstrate and evaluate the economic viability and practical usability of the MUSA framework in highly relevant industrial applications representative of multi-cloud application development potential in Europe.
The project is 36 month long. It was started in January 2015 and should have its results ready by the end of 2017.

For more information, visit [http://www.musa-project.eu/](http://www.musa-project.eu/)

## Integration information's

**WARNING! Please work on each integration on a separate branch other than *master*.**

### Dependencies installation

The overlaying application has been developed using [Typescript](https://www.typescriptlang.org/) and leveraging *angular 1.x* as a baseline framework. Full source of the compiled `app.js` files are stored under `src/` repository. 

**Minimum requirements for development**
* node 
* typings - to install type `npm install -g typings`
* typescript - to install type `npm install -g typescript`

Once requirements installed, install dependencies with: 
`typings install` followed by `npm install` and finally `git submodule init` 

There are two tasks available to compile the source: 
* `npm run tsc` - which will compile the `*.ts` files in the `src/` directory
* `npm run tsc:watch` - which will watch the files in the `src/` directory for changes and compile the `*.ts` files each time the change is detected


### Types of card view integation

Details view of the cards are implemented based on the column name, where the system is looking for a file name which is a abbriviation of the first 3 letters of the two first words of the column name from where the view was invoked. 

There are 3 types of integration possible at this stage: 
* Application deeply integrated with the main source as a sub-module of the MUSA namespace. This type of application should have it's own controller regiresterd under *MUSA* typescript namespace as. The final representation of the application should be contained withing the body of the `modal-body` element under the appropriate file from `modal_templates/` directory. Example of such inegration can be found under: `modal_templates/dep.html`
* Application stored under `musa_tools/` sub-directory. Where the application is a standalone application with it's relative `index.html` or any other representation file. The integation is done by containing the representation file within the pop up. Example of such integration can be found under: `modal_templates/mod.html`
* Application stored under another location where desired action of the details view is to redirect to the appropriate application, this can be done with or w/o redirection overload. There is a function prepared under modal controller called `redirect(url)`. This can be invoked to redirect to the appropriate url once the details view is selected. Example of such integration can be found under: `modal_templates/sla.html`

If more are needed, please reach out to [Jacek](mailto:domja01@ca.com) and we can discuss. 

## License
**The MIT License (MIT)**
Copyright (c) MUSA, 2016

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
